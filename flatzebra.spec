Name:           flatzebra
Version:        0.2.0
Release:        1%{?dist}
Summary:        Small engine for 2D video games like BurgerSpace
Summary(fr):    Petit moteur pour jeux vidéo 2D comme BurgerSpace

Group:          Development/Libraries
License:        GPLv2+
URL:            http://sarrazip.com/dev/burgerspace.html
Source0:        http://sarrazip.com/dev/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  SDL2-devel       >= 2.0.10
BuildRequires:  SDL2_image-devel >= 2.0.5
BuildRequires:  SDL2_mixer-devel >= 2.0.4
BuildRequires:  SDL2_gfx-devel   >= 1.0.2
BuildRequires:  SDL2_ttf-devel   >= 2.0.15

%description
Small game engine library required by BurgerSpace and other games.

%description -l fr
Petit moteur de jeu requis par BurgerSpace et autres jeux.


%package devel
Summary:        C++ header files for the flatzebra library
Summary(fr):    En-têtes C++ pour la librairie flatzebra.
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
C++ header files to develop with the flatzebra library.

%description -l fr devel
En-têtes C++ pour développer avec la librairie flatzebra.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT

 
%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/lib*.so.*
%{_datadir}/pixmaps/*
%doc %{_defaultdocdir}/*


%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/lib*.la
%{_libdir}/pkgconfig/*


%changelog
