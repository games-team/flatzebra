flatzebra (0.2.0-2) unstable; urgency=medium

  * Team upload.
  * Add missing font file.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 22 Sep 2024 23:20:57 +0200

flatzebra (0.2.0-1) unstable; urgency=medium

  * Team Upload
  * Package rename for ABI bump.
  * Set Rules-Requires-Root: no

  [ Patrice Duroux ]
  * New upstream version 0.2.0 (Closes: #1082538)
  * Switch from SDL1 to SDL2 (Closes: #1038346)
  * Declare compliance with Debian Policy 4.7.0.
  * Update d/copyright years.

  [ Debian Janitor ]
  * Update watch file format version to 4.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 21 Sep 2024 20:52:46 +0200

flatzebra (0.1.7-2) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.6.2.
  * Remove debian/libflatzebra-dev.lintian-overrides.
  * Override dh_missing. Only list missing files.

 -- Markus Koschany <apo@debian.org>  Sat, 25 Feb 2023 17:17:42 +0100

flatzebra (0.1.7-1) unstable; urgency=medium

  * New upstream version 0.1.7.
  * Switch to debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.1.
  * Remove obsolete Conflicts and Replaces fields in debian/control.

 -- Markus Koschany <apo@debian.org>  Sun, 17 Nov 2019 14:13:44 +0100

flatzebra (0.1.6-5) unstable; urgency=medium

  * Switch to compat level 11.
  * Use canonical VCS URI.
  * Declare compliance with Debian Policy 4.2.1.
  * Update Homepage address.
  * Update debian/watch file.

 -- Markus Koschany <apo@debian.org>  Wed, 21 Nov 2018 13:36:01 +0100

flatzebra (0.1.6-4) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.6.
  * Switch Vcs-Browser field to cgit and use https.
  * Update copyright years.
  * debian/rules: No longer export DH_OPTIONS.
  * Update my e-mail address.

 -- Markus Koschany <apo@debian.org>  Fri, 30 Oct 2015 15:00:19 +0100

flatzebra (0.1.6-3) unstable; urgency=medium

  * Team upload
  * C++ ABI transition: rename libflatzebra-0.1-2 to libflatzebra-0.1-2v5
    (starts transition for: #791035)

 -- Simon McVittie <smcv@debian.org>  Fri, 07 Aug 2015 10:43:18 +0100

flatzebra (0.1.6-2) unstable; urgency=low

  * Remove libflatzebra-0.1-2.symbols and go back to using shlibs instead.

 -- Markus Koschany <apo@gambaru.de>  Tue, 22 Jan 2013 16:52:05 +0100

flatzebra (0.1.6-1) unstable; urgency=low

  * [bde9c69] Imported Upstream version 0.1.6.
  * [e682fbb] Use dh to simplify debian/rules.
  * [70066c4] Bump Standards-Version to 3.9.4.
  * debian/control:
    - Bump compat level to 9 and require debhelper >=9.
    - Add myself to Uploaders.
    - Replace build-dependency on autotools_dev with dh-autoreconf.
    - Remove chrpath from build-depends. The library rpath works anyway.
    - Change priority from extra to optional.
  * debian/rules:
    - Build with --parallel and autoreconf.
    - Build with hardening=+all.
  * [ffd0fee] Add libflatzebra .install files.
  * [ce43b50] Add Multiarch support.
  * [91e418e] Revise package description which was interchanged.
  * [eff0ce6] Add Multi-Arch: same.
  * [df7f75a] Remove *.dirs files, we don't need them.
  * [5d7a532] Update debian/copyright to copyright format 1.0.

 -- Markus Koschany <apo@gambaru.de>  Thu, 20 Dec 2012 20:14:25 +0100

flatzebra (0.1.5-4) unstable; urgency=low

  * Team upload.
  * debian/rules: don't install la-file to dev package. (Closes: #620878)
    Thanks to Andreas Moog for the patch.

 -- Felix Geyer <debfx-pkg@fobos.de>  Mon, 23 May 2011 23:46:33 +0200

flatzebra (0.1.5-3) unstable; urgency=low

  * Remove myself from uploaders

 -- Francois Marier <francois@debian.org>  Sat, 12 Mar 2011 12:59:32 +1300

flatzebra (0.1.5-2) unstable; urgency=low

  * Bump Standards-Version up to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 16:22:29 +1300

flatzebra (0.1.5-1) unstable; urgency=low

  * New upstream release

 -- Francois Marier <francois@debian.org>  Mon, 17 May 2010 17:37:15 +1200

flatzebra (0.1.4-1) unstable; urgency=low

  * New upstream release

 -- Francois Marier <francois@debian.org>  Thu, 01 Apr 2010 08:11:43 +1300

flatzebra (0.1.3-2) unstable; urgency=low

  * Bump Standards-Version to 3.8.4
  * Add dependency on ${misc:Depends} for the dev package
  * Switch to 3.0 (quilt) source package format
  * Link to the right version of the GPL (v2) in debian/copyright

 -- Francois Marier <francois@debian.org>  Tue, 16 Feb 2010 15:00:48 +1300

flatzebra (0.1.3-1) unstable; urgency=low

  * New Upstream Version
  * Bump Standards-Version to 3.8.2
  * Bump debhelper compatibility to 7
  * debian/rules: use dh_prep instead of dh_clean -k

 -- Francois Marier <francois@debian.org>  Tue, 23 Jun 2009 12:45:21 +1200

flatzebra (0.1.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: Remove duplication "section" field (lintian notice)
  * debian/copyright: Mention copyright explicitly (lintian notice)

 -- Francois Marier <francois@debian.org>  Sat, 28 Feb 2009 00:21:27 +1300

flatzebra (0.1.1-6) unstable; urgency=low

  * Change maintainer to be the Games Team
  * Move to git and update Vcs fields

 -- Francois Marier <francois@debian.org>  Tue, 11 Nov 2008 12:34:26 +1300

flatzebra (0.1.1-5) unstable; urgency=low

  * Update libtool (closes: #493380)
  * Bump Standards-Version up to 3.8.0 (no changes)

 -- Francois Marier <francois@debian.org>  Sun, 03 Aug 2008 22:52:21 +1200

flatzebra (0.1.1-4) unstable; urgency=low

  * Bump Standards-Version up to 3.7.3 (no changes)

 -- Francois Marier <francois@debian.org>  Tue, 22 Jan 2008 16:15:47 +1300

flatzebra (0.1.1-3) unstable; urgency=low

  * Rename the XS-Vcs-* fields to Vcs-*

 -- Francois Marier <francois@debian.org>  Tue, 20 Nov 2007 15:41:08 +1300

flatzebra (0.1.1-2) unstable; urgency=low

  * Add pkg-config to the build depends (closes: #451661)

 -- Francois Marier <francois@debian.org>  Mon, 19 Nov 2007 16:50:38 +1300

flatzebra (0.1.1-1) unstable; urgency=low

  * Initial release (Closes: #448805)

 -- Francois Marier <francois@debian.org>  Tue, 13 Nov 2007 14:50:56 +1300
