/*  Stats.h

    roundbeetle - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <float.h>
#include <algorithm>


class Stats
{
public:
    Stats()
    :   numValues(0),
        minValues(),
        maxValues(),
        sumsOfValues()
    {
        reset();
    }

    void reset()
    {
        numValues = 0;
        minValues[0] = minValues[1] = FLT_MAX;
        maxValues[0] = maxValues[1] = - FLT_MAX;
        sumsOfValues[0] = sumsOfValues[1] = 0;
    }

    void add(float leftValue, float rightValue)
    {
        ++numValues;
        minValues[0] = std::min(minValues[0], leftValue);
        maxValues[0] = std::max(maxValues[0], leftValue);
        sumsOfValues[0] += leftValue;
        minValues[1] = std::min(minValues[1], rightValue);
        maxValues[1] = std::max(maxValues[1], rightValue);
        sumsOfValues[1] += rightValue;
    }

    float getInterval(bool rightSpeaker = false) const
    {
        size_t index = (rightSpeaker ? 1 : 0);
        return maxValues[index] - minValues[index];
    }

    float getAverage(bool rightSpeaker = false) const
    {
        size_t index = (rightSpeaker ? 1 : 0);
        return numValues == 0 ? HUGE_VALF : sumsOfValues[index] / numValues;
    }

    float numValues;
    float minValues[2];  // index 0 if left speaker, 1 is right
    float maxValues[2];
    float sumsOfValues[2];
};
