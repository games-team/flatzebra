/*  RendererTest.h

    roundbeetle - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include "Test.h"

#include <set>


namespace roundbeetle
{
    class SampleSource;
    class FrameSource;
    class FrameSourceAdder;
    class PausableSource;
    class VolumeMeter;
};

class Stats;

class SDL_mutex;


// Abstract class.
//
class RendererTest : public Test
{
public:

    enum { rendererFreq = 44100 };

    // preMeterAdder --> pausableSource --> volumeMeter --> TopFrameSourceAdder
    //
    RendererTest(const char *testName);

    virtual ~RendererTest();

    virtual void run(bool mute) override = 0;

    // Writes to cout.
    // Returns success/failure.
    //
    static bool convertRawToWave(const char *rawFilename, const char *waveFilename, int rawFreq);

protected:

    bool setup(bool mute);

    bool cleanup();

protected:

    void showRMS(const Stats &stats);

    void expectAverageRMS(const Stats &stats, float expectedAverageRMS);

    void expectRMSRange(const Stats &stats, float minRMS, float maxRMS, bool rightSpeaker = false);

    static void onChildRemoved(roundbeetle::FrameSource *child, void *userData, roundbeetle::FrameSourceAdder * /*adder*/);

    void checkNoChildrenInRemovedList();

    // Returns elapsed time in seconds.
    //
    float playPanner(const char *desc,
                     roundbeetle::SampleToFramePanner *panner,
                     Stats *stats,
                     float maxPlayTimeInSeconds = 0);

    // Returns elapsed time in seconds.
    //
    float playSampleSource(const char *desc,
                           roundbeetle::SampleSource *ss,
                           Stats *stats,
                           float maxPlayTimeInSeconds = 0);

    // Asserts the given expectations.
    //
    void playSampleSource(const char *desc,
                            roundbeetle::SampleSource *ss,
                            float expectedDuration,
                            float delayBeforeRMSMeasurement,
                            float expectedLeftRMS,
                            float expectedRightRMS);

    void playSampleSourcePair(const char *desc,
                              roundbeetle::SampleSource *ss0, roundbeetle::SampleSource *ss1,
                              Stats *stats);

    // For stable RMS, sound should play for more than 1.5 second.
    //
    void waitForChildRemoval(roundbeetle::FrameSource *child,
                             Stats *stats,
                             float maxPlayTimeInSeconds = 0);

    // If 'child' in 'removedChildren', removes it and returns true.
    // Otherwise, returns false.
    //
    bool hasChildBeenRemoved(roundbeetle::FrameSource *child);

    float getLeftRMS() const;

    void measureRMS(Stats &stats, float durationInSeconds);

private:

    RendererTest(const RendererTest &);
    RendererTest &operator = (const RendererTest &);

protected:

    const char *recordingFilename;
    const char *recordingWaveFilename;
    SDL_mutex *mutex;
    roundbeetle::FrameSourceAdder *preMeterAdder;
    roundbeetle::PausableSource *pausableSource;
    roundbeetle::VolumeMeter *volumeMeter;
    std::set<roundbeetle::FrameSource *> removedChildren;  // must be accessed under mutex

};
