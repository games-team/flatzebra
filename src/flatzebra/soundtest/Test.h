/*  Test.h

    roundbeetle - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <roundbeetle/SampleToFramePanner.h>

#include <string>


class Test
{
public:

    Test(const std::string &_name)
    :   name(_name),
        monoToStereoAttenuation(roundbeetle::SampleToFramePanner::monoToStereoAttenuation)
    {
    }

    virtual ~Test()
    {
    }

    virtual std::string getName() const
    {
        return name;
    }

    virtual void run(bool mute) = 0;

protected:

    std::string name;
    const float monoToStereoAttenuation;

};
