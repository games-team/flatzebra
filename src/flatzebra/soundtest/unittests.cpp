/*  roundbeetle - SDL-based sound renderer
    Copyright (C) 2011-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/NativeSDLSoundRenderer.h>
#include <roundbeetle/SoundEngine.h>
#include <roundbeetle/SineSource.h>
#include <roundbeetle/WhiteNoiseSource.h>
#include <roundbeetle/SquareWaveSource.h>
#include <roundbeetle/PausableSource.h>
#include <roundbeetle/VolumeMeter.h>
#include <roundbeetle/WaveFileSource.h>
#include <roundbeetle/Fader.h>
#include <roundbeetle/SampleToFramePanner.h>
#include <roundbeetle/LoopingSource.h>
#include <roundbeetle/ADSRSource.h>
#include <roundbeetle/ADSR.h>
#include <roundbeetle/ClientObject.h>
#include <roundbeetle/FrequencyFunction.h>

#include "RendererTest.h"
#include "Stats.h"

#include <SDL2/SDL.h>

#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <set>
#include <map>
#include <sys/time.h>
#include <stdint.h>

#ifdef HAVE_GETOPT_LONG
#include <getopt.h>
#endif

using namespace std;
using namespace roundbeetle;


#define SHOWNL(expr) (std::cout << #expr << "=" << (expr) << std::endl)


class PosTest : public RendererTest
{
public:

    PosTest()
    :   RendererTest("pos")
    {
    }

    virtual void run(bool mute) override
    {
        if (! setup(mute))
            assert(!"setup() failed");

        const float rmsTolerance = .001f;

        const float yFor10Deg = float(tan(M_PI / 180 * 10));

        struct
        {
            float x, y, z;
            float leftRMS, rightRMS;
        } tests[] =
        {
            {   0,   0,   0,  .212f,  .212f },  // at mic
            {   0,   1,   0,  .212f,  .212f },  // in front of mic
            {   0,  -1,   0,  .212f,  .212f },  // behind mic
            {  -1,   0,   0,  .300f,  .000f },  // left of mic
            {   1,   0,   0,  .000f,  .300f },  // right of mic

            {   1,   1,   0,  .115f,  .277f },  // 45 deg front right
            {  -1,   1,   0,  .277f,  .115f },  // 45 deg front left
            {   1,  -1,   0,  .115f,  .277f },  // 45 deg back right
            {  -1,  -1,   0,  .277f,  .115f },  // 45 deg back left

            {   1,   yFor10Deg,   0,  .026f,  .298f },  // 10 deg front right
            {  -1,   yFor10Deg,   0,  .298f,  .026f },  // 10 deg front left
            {   1,  -yFor10Deg,   0,  .026f,  .298f },  // 10 deg back right
            {  -1,  -yFor10Deg,   0,  .298f,  .026f },  // 10 deg back left
        };

        ClientObject clObj(true, Vec(0, 0, 0));

        for (size_t i = 0; i < sizeof(tests) / sizeof(tests[0]); ++i)
        {
            clObj.setPos(tests[i].x, tests[i].y, tests[i].z);  // locks/unlocks renderer mutex

            SampleSource *ss = new SquareWaveSource(220, .3f, 0);
            SampleToFramePanner *panner = new SampleToFramePanner(ss, &clObj);
            cout << "square wave positioned at (" << tests[i].x << ", " << tests[i].y << ", " << tests[i].z << ")" << endl;

            checkNoChildrenInRemovedList();
            preMeterAdder->addChild(panner);

            SDL_Delay(300);
            float actualLeftRMS, actualRightRMS;
            volumeMeter->getCurrentRMSVolumes(actualLeftRMS, actualRightRMS);
            cout << "Left RMS = " << actualLeftRMS << ", Right RMS = " << actualRightRMS << endl;
            assert(fabsf(actualLeftRMS  - tests[i].leftRMS)  < rmsTolerance);
            assert(fabsf(actualRightRMS - tests[i].rightRMS) < rmsTolerance);

            if (! preMeterAdder->killChild(panner))
                assert("killChild failed");

            waitForChildRemoval(panner, NULL, 1);
            checkNoChildrenInRemovedList();
        }

        // Test with changing position.
        //
        {
            size_t testLen = 3;  // seconds
            Uint32 frameLen = 16;  // ms
            size_t numFrames = testLen * (1000 / frameLen);
            float xStart = -3, xEnd = +3;
            cout << endl;
            cout << "Square wave moving from x=" << xStart << " to x=" << xEnd << " over " << testLen << " s" << endl;

            checkNoChildrenInRemovedList();
            SampleToFramePanner *panner = NULL;

            float prevLeftRMS = 0, prevRightRMS = 0;
            float leftRMS = 0, rightRMS = 0;

            for (size_t i = 0; i <= numFrames; ++i)
            {
                float x = (i / float(numFrames)) * (xEnd - xStart) + xStart;
                //cout << "i=" << i << ": x=" << x << endl;
                clObj.setPos(x, 1, 0);  // locks/unlocks renderer mutex

                if (panner == NULL)  // start sound at 1st frame
                {
                    SampleSource *ss = new SquareWaveSource(220, .3f, 0);
                    panner = new SampleToFramePanner(ss, &clObj);

                    preMeterAdder->addChild(panner);
                    SDL_Delay(200);  // allow at least one RMS window to pass
                }

                SDL_Delay(frameLen);

                if (i % 13 == 12)  // each 208 ms, new RMS window computed
                {
                    volumeMeter->getCurrentRMSVolumes(leftRMS, rightRMS);

                    float leftDelta  = leftRMS  - prevLeftRMS;
                    float rightDelta = rightRMS - prevRightRMS;
                    cout << "x = " << x << ", Left RMS = " << leftRMS << " (" << leftDelta
                         << "), Right RMS = " << rightRMS << " (" << rightDelta << ")"
                         << endl;

                    if (prevLeftRMS == 0)  // if 1st measurement
                    {
                        assert(fabsf(leftRMS  - .295f) < .005f);
                        assert(fabsf(rightRMS - .053f) < .005f);
                    }
                    else
                    {
                        assert(leftDelta  < -4e-4);
                        assert(rightDelta > +4e-4);
                    }

                    prevLeftRMS  = leftRMS;
                    prevRightRMS = rightRMS;
                }
            }

            assert(fabsf(leftRMS  - .052f) < .005f);
            assert(fabsf(rightRMS - .295f) < .005f);

            if (! preMeterAdder->killChild(panner))
                assert("killChild failed");

            waitForChildRemoval(panner, NULL, 1);
            checkNoChildrenInRemovedList();
        }

        if (! cleanup())
            assert(!"cleanup() failed");
    }

private:

    PosTest(const PosTest &);
    PosTest &operator = (const PosTest &);

};


class BasicTest : public RendererTest
{
public:

    BasicTest()
    :   RendererTest("basic")
    {
    }

    virtual void run(bool mute) override
    {
        if (! setup(mute))
            assert(!"setup() failed");

        // Source: http://en.wikipedia.org/wiki/Root_mean_square#RMS_of_common_waveforms
        const float sineAmplitude = 0.3f;  // i.e., linear volume of the sine
        const float monoSineAmplitude = sineAmplitude * monoToStereoAttenuation;
        const float expectedMonoSineRMS = monoSineAmplitude / sqrtf(2);

        Stats stats;
        float elapsedSeconds = 0;

        cout << "Checking that RMS returns 0 on silence\n";
        measureRMS(stats, 1);
        showRMS(stats);
        expectRMSRange(stats, 0, 0);


        playSampleSource("white noise, linear fade-out",
                         new Fader(new WhiteNoiseSource(0.5f, 2), .1f, 1, Fader::LINEAR),
                         &stats);
        showRMS(stats);
        expectRMSRange(stats, 0.010f, 0.200f);

        playSampleSource("white noise, non-linear fade-out",
                         new Fader(new WhiteNoiseSource(0.5f, 2), 0.1f, 1, Fader::INV_EXP_4),
                         &stats);
        showRMS(stats);
        expectRMSRange(stats, 0.010f, 0.200f);

        elapsedSeconds = playSampleSource("5 repetitions of white noise, non-linear fade-out",
                         new LoopingSource(
                             new Fader(new WhiteNoiseSource(0.5f, 0.5f), 0, 0.5f, Fader::INV_EXP_4),
                             5),
                         &stats);
        assert(elapsedSeconds >= 2.5f && elapsedSeconds <= 2.9f);
        showRMS(stats);
        expectRMSRange(stats, 0.003f, 0.150f);

        elapsedSeconds = playSampleSource("stop after 2 seconds of infinite repetitions of white noise, non-linear fade-out",
                                 new LoopingSource(
                                     new Fader(new WhiteNoiseSource(0.5f, 0.5f), 0, 0.5f, Fader::INV_EXP_4),
                                     0 /*infinite*/),
                                 &stats,
                                 2);
        assert(elapsedSeconds >= 2.0f && elapsedSeconds <= 2.3f);


        playSampleSource("white noise, no fade-out",
                         new WhiteNoiseSource(0.5f, 2),
                         &stats);
        showRMS(stats);
        expectRMSRange(stats, 0.185f, 0.215f);


        elapsedSeconds = playSampleSource("stop after 1 second of white noise, no fade-out",
                         new WhiteNoiseSource(0.5f, 10),
                         &stats,
                         1);
        assert(elapsedSeconds >= 1.0f && elapsedSeconds <= 1.3f);


        // This loads the .wav and keeps its contents.
        // It can be played more than once through a WaveFileSource object.
        //
        WaveFileBuffer waveBuf0(WAVDIR "/level-finished.wav");

        for (int i = 2; i--; )
        {
            float elapsedSeconds = playSampleSource("wave file", new WaveFileSource(waveBuf0), &stats);
            assert(elapsedSeconds > 2.5f);
            showRMS(stats);
            expectRMSRange(stats, 0.070f, 0.130f);
        }

        playSampleSource("sine wave",
                         new SineSource(220, sineAmplitude, 2),
                         &stats);
        showRMS(stats);
        expectAverageRMS(stats, expectedMonoSineRMS);


        playSampleSourcePair("two equal sine waves in same adder",
                             new SineSource(220, sineAmplitude, 2),
                             new SineSource(220, sineAmplitude, 2),
                             &stats);
        showRMS(stats);
        expectAverageRMS(stats, expectedMonoSineRMS * 2);

        testPausableSource(sineAmplitude, expectedMonoSineRMS);


        cout << endl;
        cout << "Checking that RMS returns 0 on silence after using adder\n";
        measureRMS(stats, 1);
        showRMS(stats);
        expectRMSRange(stats, 0, 0);

        if (! cleanup())
            assert(!"cleanup() failed");
    }

private:

    void testPausableSource(float sineAmplitude, float expectedMonoSineRMS)
    {
        cout << endl;
        cout << "Playing: sine with pause" << endl;

        checkNoChildrenInRemovedList();
        SineSource *ss = new SineSource(220, sineAmplitude, 4);
        SampleToFramePanner *panner = new SampleToFramePanner(ss, NULL);

        // Start sine.
        //cout << "- RendererTest::testPausableSource: adding child " << panner << endl;
        preMeterAdder->addChild(panner);

        // Check that sine is playing.
        SDL_Delay(500);
        float leftRMS = getLeftRMS();
        cout << "During sine: left RMS = " << leftRMS
             << " (expecting " << expectedMonoSineRMS << ")" << endl;
        assert(fabsf(leftRMS - expectedMonoSineRMS) < 0.001f);

        // Pause and check that the sine is not playing.
        pausableSource->pause();

        SDL_Delay(500);
        leftRMS = getLeftRMS();
        cout << "After 1st pause() call: left RMS = " << leftRMS << endl;
        assert(leftRMS == 0);

        // Pause again and check that the sine is still not playing.
        pausableSource->pause();

        SDL_Delay(500);
        leftRMS = getLeftRMS();
        cout << "After 2nd pause() call: left RMS = " << leftRMS << endl;
        assert(leftRMS == 0);

        // Resume once and check that the sine is still not playing.
        pausableSource->resume();

        SDL_Delay(500);
        leftRMS = getLeftRMS();
        cout << "After 1st resume() call: left RMS = " << leftRMS << endl;
        assert(leftRMS == 0);

        // Resume again and check that the sine is playing.
        pausableSource->resume();

        SDL_Delay(500);
        leftRMS = getLeftRMS();
        cout << "After 2nd resume() call: left RMS = " << leftRMS << endl;
        assert(fabsf(leftRMS - expectedMonoSineRMS) < 0.001f);

        //cout << "- RendererTest::testPausableSource: killing child " << panner << endl;
        if (! preMeterAdder->killChild(panner))
            assert("killChild failed");

        waitForChildRemoval(panner, NULL, 1);
        checkNoChildrenInRemovedList();

        //cout << "- RendererTest::testPausableSource: end" << endl;
    }

private:

    BasicTest(const BasicTest &);
    BasicTest &operator = (const BasicTest &);

};


class SquareWaveTest : public RendererTest
{
public:

    SquareWaveTest()
    :   RendererTest("squarewave")
    {
    }

    virtual void run(bool mute) override
    {
        if (! setup(mute))
            assert(!"setup() failed");

        const float squareAmplitude = 0.3f;
        const float monoSquareAmplitude = squareAmplitude * monoToStereoAttenuation;
        const float expectedMonoSquareRMS = monoSquareAmplitude;

        Stats stats;
        float elapsedSeconds = 0;

        playSampleSource("square wave, no fade-out",
                                          new SquareWaveSource(220, squareAmplitude, 1),
                                          1, .8f, expectedMonoSquareRMS, expectedMonoSquareRMS);


        {
            SampleSource *src = new SquareWaveSource(220, .3f, 10);

            ADSR adsr(.9f, .6f, 1, .5f, 1.5f, .75f);

            float totalTime = adsr.getTotalTime();

            ADSRSource *as = new ADSRSource(src, adsr);
            elapsedSeconds = playSampleSource("ADSR on square wave", as, &stats);
            showRMS(stats);
            cout << "ADSR time   : " << totalTime << " s" << endl;
            assert(fabsf(elapsedSeconds - totalTime) < .3f);
            expectRMSRange(stats, 0.0086f, 0.250f);
        }

        {
            class ConstantFreq : public FrequencyFunction
            {
            public:
                virtual size_t getNumSamplesPerPeriod(size_t /*sampleCounter*/) override
                {
                    return SoundRenderer::freq() / 220;
                }
            };

            ConstantFreq cf;
            SquareWaveSource *sq0 = new SquareWaveSource(cf, .3f, 1);
            playSampleSource("220 Hz square wave using FrequencyFunction", sq0,
                             1, .8f, monoSquareAmplitude, monoSquareAmplitude);

            const float totalTimeInS = 1;
            LinearMovingFreq mf(880, 110, totalTimeInS);  // decreasing tone
            SquareWaveSource *sq1 = new SquareWaveSource(mf, .3f, totalTimeInS);
            playSampleSource("square wave with decreasing freq. using FrequencyFunction", sq1,
                             totalTimeInS, totalTimeInS - .2f, monoSquareAmplitude, monoSquareAmplitude);
        }

        if (! cleanup())
            assert(!"cleanup() failed");
    }

};


uint64_t
getTimeOfDay()
{
    timeval tv;
    gettimeofday(&tv, NULL);
    return uint64_t(tv.tv_sec) * 1000000 + uint64_t(tv.tv_usec);
}


class EngineTest : public Test
{
public:

    EngineTest()
    :   Test("engine")
    {
    }

    virtual ~EngineTest()
    {
    }

    virtual void run(bool mute) override
    {
        const int rendererFreq = 44100;
        const char *rawRecordingFilename = "recorded.raw";
        int status = SoundEngine::create(rendererFreq, .1f, rawRecordingFilename);
        assert(status == 0);
        SoundEngine &engine = SoundEngine::instance();
        Bus &mainBus = engine.getMainBus();

        cout << "Checking that requesting a sound before postBusInit fails" << endl;
        assert(engine.requestSine(220, .1f, 1, mainBus) == -1);

        cout << "Calling postBusInit." << endl;
        engine.postBusInit();

        cout << endl;

        // Upon initialization, SDL audio invokes the callback about 8 times
        // without delay. (Then it invokes it once every 10 ms as requested.)
        // We wait out these initial invocations so that the timing assertions
        // below are accurate.
        //
        SDL_Delay(100);

        if (mute)
            SoundRenderer::instance().getTopFrameSourceAdder().setLinearAttenuation(0);


        cout << "Playing square wave with ADSR and increasing frequency." << endl;

        float elapsedSeconds = 0;
        int sqWaveHandle = -1;
        Uint32 startTimeMS = 0;
        size_t frameNo = 0;
        size_t numIters = 0;
        enum { FPS = 60 };

        ADSR adsr(.4f, .2f, .5f, .5f, .5f, .5f);
        cout << "Total time: " << adsr.getTotalTime() << " s" << endl;
        startTimeMS = SDL_GetTicks();
        cout << "startTimeMS=" << startTimeMS << endl;
        LinearMovingFreq mf0(220, 440, adsr.getTotalTime());
        sqWaveHandle = engine.requestSquareWave(mf0, adsr, 1, mainBus);
        while (engine.isRequestAlive(sqWaveHandle))
            SDL_Delay(1000 / FPS);
        Uint32 endTimeMS = SDL_GetTicks();
        elapsedSeconds = (endTimeMS - startTimeMS) / 1000.0f;
        cout << endTimeMS << " - " << startTimeMS << endl;
        cout << "Duration: " << elapsedSeconds << " s" << endl;
        assert(elapsedSeconds >= adsr.getTotalTime() - .012f);
        assert(elapsedSeconds <= adsr.getTotalTime() + .100f);
        cout << endl;
        SDL_Delay(200);


        cout << "Playing 4 loops of square wave with ADSR and decreasing frequency." << endl;

        ADSR adsr1(.4f, .2f, .2f, .2f, .2f, .2f);
        numIters = 4;
        cout << "Number of iterations: " << numIters << endl;
        LinearMovingFreq mf1(880, 110, adsr1.getTotalTime());
        sqWaveHandle = engine.requestSquareWave(mf1, adsr1, numIters, mainBus);
        while (engine.isRequestAlive(sqWaveHandle))
            SDL_Delay(1000 / FPS);
        cout << endl;
        SDL_Delay(200);


        cout << "Playing 4 loops of white noise with ADSR." << endl;

        ADSR adsr2(.4f, .2f, .0f, .0f, .0f, .4f);
        numIters = 4;
        cout << "Number of iterations: " << numIters << endl;
        int whiteNoiseHandle = engine.requestWhiteNoise(adsr2, numIters, mainBus);
        cout << "Single iteration time: " << adsr2.getTotalTime() << " s" << endl;
        startTimeMS = SDL_GetTicks();
        for (frameNo = 0; engine.isRequestAlive(whiteNoiseHandle); ++frameNo)
            SDL_Delay(1000 / FPS);
        cout << "Duration: " << frameNo / float(FPS) << " s" << endl;
        cout << endl;
        SDL_Delay(200);


        cout << "Playing wave file:" << endl;
        WaveFileBuffer wfb(WAVDIR "/level-finished.wav");

        int reqHandle = engine.requestWaveFileSound(wfb, mainBus);
        assert(reqHandle != -1);

        size_t numFrames = 0;
        for (numFrames = 0; engine.isRequestAlive(reqHandle); ++numFrames)
        {
            SDL_Delay(50);
        }

        cout << "Request " << reqHandle << " finished after " << numFrames << " frame(s)" << endl;
        assert(numFrames >= 50 && numFrames <= 56);

        cout << endl;
        cout << "Playing sine wave:" << endl;
        const float sineAmplitude = 0.3f;  // i.e., linear volume of the sine
        const float monoSineAmplitude = sineAmplitude * monoToStereoAttenuation;
        const float expectedMonoSineRMS = monoSineAmplitude / sqrtf(2);

        int sineReq = engine.requestSine(220, sineAmplitude, 999, mainBus);
        SDL_Delay(300);
        cout << "Checking RMS of sine" << endl;
        expectRMS(mainBus, expectedMonoSineRMS);

        // Test pause/resume on request.
        //
        for (int iter = 1; iter <= 3; ++iter)
        {
            assert(engine.isRequestAlive(sineReq));

            cout << "Pausing request" << endl;
            if (! engine.pauseRequest(sineReq))
                assert(!"pauseRequest failed");
            SDL_Delay(300);
            cout << "Checking that RMS is zero during pause" << endl;
            expectRMS(mainBus, 0);

            assert(engine.isRequestAlive(sineReq));

            cout << "Resuming request" << endl;
            if (! engine.resumeRequest(sineReq))
                assert(!"pauseRequest failed");
            SDL_Delay(300);
            cout << "Checking that RMS is back up after resuming" << endl;
            expectRMS(mainBus, expectedMonoSineRMS);
        }

        // Test pause/resume on main bus.
        //
        for (int iter = 1; iter <= 3; ++iter)
        {
            assert(engine.isRequestAlive(sineReq));

            cout << "Pausing main bus" << endl;
            if (! mainBus.pause())
                assert(!"pauseRequest failed");
            SDL_Delay(300);
            cout << "Checking that RMS is zero during pause" << endl;
            expectRMS(mainBus, 0);

            assert(engine.isRequestAlive(sineReq));

            cout << "Resuming main bus" << endl;
            if (! mainBus.resume())
                assert(!"pauseRequest failed");
            SDL_Delay(300);
            cout << "Checking that RMS is back up after resuming" << endl;
            expectRMS(mainBus, expectedMonoSineRMS);
        }

        //TODO: test pauseEngine()/resumeEngine().

        assert(engine.isRequestAlive(sineReq));
        cout << "Stopping sine" << endl;
        if (! engine.stopRequest(sineReq))
            assert(!"stopRequest failed");
        assert(! engine.isRequestAlive(sineReq));
        SDL_Delay(300);
        cout << "Checking that RMS is zero after stopping" << endl;
        expectRMS(mainBus, 0);


        SoundEngine::destroy();

        if (! RendererTest::convertRawToWave(rawRecordingFilename, "recorded.wav", rendererFreq))
            assert("failed to convert .raw to .wav");
    }

private:

    void expectRMS(Bus &bus, float expectedRMS)
    {
        assert(bus.getVolumeMeter() != NULL);
        float leftLinVol, rightLinVol;
        bus.getVolumeMeter()->getCurrentRMSVolumes(leftLinVol, rightLinVol);

        cout << "RMS volumes: " << leftLinVol << ", " << rightLinVol << endl;
        assert(fabsf(leftLinVol - expectedRMS) <= .001f);
        assert(fabsf(rightLinVol - expectedRMS) <= .001f);
    }

};


class GeneralTest : public Test
{
public:

    GeneralTest()
    :   Test("general")
    {
    }

    virtual ~GeneralTest()
    {
    }

    typedef map<int, FrequencyFunction *> RequestMap;

    static RequestMap requestMap;

    // This callback will be invoked under the renderer mutex.
    //
    static void onRequestFinished(int reqHandle, void *)
    {
        RequestMap::iterator it = requestMap.find(reqHandle);
        if (it == requestMap.end())
            return;
        assert(it->second != NULL);
        //cout << "onRequestFinished(" << reqHandle << "): " << it->second << endl;
        delete it->second;
        requestMap.erase(it);
    }

    virtual void run(bool mute) override
    {
        const int rendererFreq = 44100;

        int status = SoundEngine::create(rendererFreq, 0, NULL);
        if (status != 0)
        {
            assert(!"SoundEngine::create() failed");
            return;
        }
        SoundEngine &engine = SoundEngine::instance();
        engine.registerRequestFinishedCallback(onRequestFinished, NULL);
        engine.postBusInit();

        if (mute)
            SoundRenderer::instance().getTopFrameSourceAdder().setLinearAttenuation(0);

        cout << "Playing square wave with ADSR and increasing frequency." << endl;

        ADSR adsr(.4f, .2f, .5f, .5f, .5f, .5f);

        LinearMovingFreq *ff = new LinearMovingFreq(220, 440, adsr.getTotalTime());

        Bus &mainBus = engine.getMainBus();
        int sqWaveHandle = engine.requestSquareWave(*ff, adsr, 1, mainBus);

        if (sqWaveHandle == -1)  // if failed
        {
            delete ff;  // onRequestFinished() will not be invoked in this case
            assert(!"requestSquareWave() failed");
            return;
        }
        requestMap[sqWaveHandle] = ff;

        cout << "Request handle: " << sqWaveHandle << endl;
        const float totalTimeMS = (.5f * 4) * 1000.0f;
        cout << "Total time: " << totalTimeMS << " ms" << endl;
        enum { FPS = 60 };

        while (engine.isRequestAlive(sqWaveHandle))
            SDL_Delay(1000 / FPS);
        SDL_Delay(200);

        SoundEngine::destroy();
    }

};


GeneralTest::RequestMap GeneralTest::requestMap;


#ifdef HAVE_GETOPT_LONG
static struct option knownOptions[] =
{
    { "help",          no_argument,       NULL, 'h' },
    { "version",       no_argument,       NULL, 'v' },
    { "unmute",        no_argument,       NULL, 'u' },
    { "list",          no_argument,       NULL, 'l' },

    { NULL, 0, NULL, 0 }  // marks the end
};


static
void
displayVersionNo()
{
    cout << PACKAGE << " unit tests " << VERSION << endl;
}


static
void
displayHelp()
{
    cout << "\n";

    displayVersionNo();

    cout <<
"\n"
"Usage: unittests [options] [test names]\n"
"\n"
"If no test names are specified, all of the known tests are run.\n"
"\n"
"Known options:\n"
"--help             Display this help page and exit\n"
"--version          Display this program's version number and exit\n"
"--unmute           Generate no sound in the sound device\n"
"--list             List names of known unit tests\n"
"\n"
    ;
}
#endif  /* HAVE_GETOPT_LONG */


int
main(int argc, char *argv[])
{
    if (setenv("DISPLAY", "", 1) != 0)
        assert(!"setenv('DISPLAY') failed");

    bool mute = true;
    bool listTestNames = false;

#ifdef HAVE_GETOPT_LONG
    int c;
    do
    {
        c = getopt_long(argc, argv, "", knownOptions, NULL);

        switch (c)
        {
            case EOF:
                break;  // nothing to do

            case 'v':
                displayVersionNo();
                return EXIT_SUCCESS;

            case 'h':
                displayHelp();
                return EXIT_SUCCESS;

            case 'u':
                mute = false;
                break;

            case 'l':
                listTestNames = true;
                break;

            default:
                displayHelp();
                return EXIT_FAILURE;
        }
    } while (c != EOF && c != '?');

#else
    int optind = 1;
#endif  /* HAVE_GETOPT_LONG */


    try
    {
        set<string> testsToRun;  // empty means all of them
        for (int i = optind; i < argc; ++i)
        {
            string reqTestName(argv[i]);
            testsToRun.insert(reqTestName);
        }

        if (SDL_Init(SDL_INIT_TIMER) != 0)  // to allow calls to SDL_GetTicks() outside of unit tests
            assert(!"SDL_Init(SDL_INIT_TIMER) failed");

        std::vector<Test *> testList;
        testList.push_back(new PosTest());
        testList.push_back(new GeneralTest());
        testList.push_back(new SquareWaveTest());
        testList.push_back(new EngineTest());
        testList.push_back(new BasicTest());

        size_t numTestsRun = 0;

        for (std::vector<Test *>::iterator it = testList.begin();
                                          it != testList.end(); ++it)
        {
            const string &testName = (*it)->getName();

            if (listTestNames)
                cout << testName << "\n";
            else
            {
                if (testsToRun.size() > 0 && testsToRun.find(testName) == testsToRun.end())
                    continue;  // current test not requested by user

                cout << endl;
                cout << "==== STARTING TEST: \"" << testName << "\" ====" << endl;
                cout << endl;
                Uint32 testStartTimeMS = SDL_GetTicks();
                (*it)->run(mute);
                Uint32 testEndTimeMS = SDL_GetTicks();
                cout << endl;
                assert(testStartTimeMS <= testEndTimeMS);  // this can fail if SDL_GetTicks() called before re-initializing SDL
                float testLenS = (testEndTimeMS - testStartTimeMS) / 1000.0f;
                cout << "==== FINISHED TEST: \"" << testName
                     << "\" after " << testLenS << " s ====" << endl;
                cout << endl;

                ++numTestsRun;
            }
        }

        for (std::vector<Test *>::iterator it = testList.begin();
                                          it != testList.end(); ++it)
            delete *it;

        testList.clear();
        SDL_Quit();

        cout << endl;
        cout << "Number of tests run: " << numTestsRun << endl;
        if (testsToRun.size() > 0 && numTestsRun < testsToRun.size())
        {
            cout << "ERROR: only " << numTestsRun << " test(s) run but "
                 << testsToRun.size() << " requested" << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }
    catch (const std::string &msg)
    {
        std::cerr << "main: exception: " << msg << "\n";
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "main: uncaught exception\n";
        return EXIT_FAILURE;
    }
}
