/*  RendererTest.cpp

    roundbeetle - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include "RendererTest.h"

#include <roundbeetle/FrameSourceAdder.h>
#include <roundbeetle/PausableSource.h>
#include <roundbeetle/VolumeMeter.h>
#include <roundbeetle/NativeSDLSoundRenderer.h>
#include <roundbeetle/SineSource.h>
#include <roundbeetle/SampleToFramePanner.h>

#include "Stats.h"

#include <SDL2/SDL.h>

#include <sstream>

using namespace roundbeetle;
using namespace std;


RendererTest::RendererTest(const char *testName)
:   Test(testName),
    recordingFilename("recorded.raw"),
    recordingWaveFilename("recorded.wav"),
    mutex(SDL_CreateMutex()),
    preMeterAdder(NULL),
    pausableSource(NULL),
    volumeMeter(NULL),
    removedChildren()
{
}


//virtual
RendererTest::~RendererTest()
{
    SDL_DestroyMutex(mutex);
}


// preMeterAdder --> pausableSource --> volumeMeter --> TopFrameSourceAdder
//
bool
RendererTest::setup(bool mute)
{
    NativeSDLSoundRenderer &soundRenderer = NativeSDLSoundRenderer::create(rendererFreq);

    if (mute)
        soundRenderer.getTopFrameSourceAdder().setLinearAttenuation(0);

    std::cout << "Recording frames in file " << recordingFilename << std::endl;
    soundRenderer.openRecordingFile(recordingFilename);
    soundRenderer.startRecording();

    int status = soundRenderer.start();
    if (status != 0)
    {
        std::cerr << "SoundRenderer: status = " << status << std::endl;
        assert(!"Sound renderer creation failed");
        return false;
    }


    preMeterAdder = new FrameSourceAdder(1);
    pausableSource = new PausableSource(preMeterAdder);
    volumeMeter = new VolumeMeter(0.1f, pausableSource);
    preMeterAdder->registerChildRemovedCallback(onChildRemoved, this);
    SoundRenderer::instance().getTopFrameSourceAdder().addChild(volumeMeter);
    SoundRenderer::instance().getTopFrameSourceAdder().registerChildRemovedCallback(onChildRemoved, this);

    return true;
}


bool
RendererTest::cleanup()
{
    checkNoChildrenInRemovedList();

    NativeSDLSoundRenderer &soundRenderer = dynamic_cast<NativeSDLSoundRenderer &>(SoundRenderer::instance());
    soundRenderer.getTopFrameSourceAdder().killChild(volumeMeter);


    soundRenderer.stopRecording();
    if (! soundRenderer.closeRecordingFile())
        assert(!"failed to close recording file");

    cout << endl;
    std::cout << "Stopping the renderer." << std::endl;
    soundRenderer.requestStop();
    std::cout << "Waiting on sound renderer thread:" << std::endl;
    soundRenderer.waitForEnd();
    std::cout << "Sound renderer thread finished." << std::endl;
    std::cout << "Destroying renderer." << std::endl;
    NativeSDLSoundRenderer::destroy();

    return convertRawToWave(recordingFilename, recordingWaveFilename, rendererFreq);
}


bool  //static
RendererTest::convertRawToWave(const char *rawFilename, const char *waveFilename, int rawFreq)
{
    stringstream cmd;
    cmd << "sox -r " << rawFreq << " -c 2 -b 16 -e signed-integer "
            << rawFilename << " " << waveFilename;
    cout << "Converting recorded samples file to wave file.\n";
    cout << "Command: " << cmd.str() << endl;
    if (system(cmd.str().c_str()) != 0)
    {
        std::cout << "ERROR: failed to convert recorded samples file to wave file with command: "
                  << cmd.str() << "\n";
        return false;
    }

    cout << "Wave file " << waveFilename << " successfully created.\n";
    return true;
}


void
RendererTest::showRMS(const Stats &stats)
{
    cout << "RMS volume: " << stats.numValues << " measurements\n";
    if (stats.numValues != 0)
    {
        float avgRMS = stats.getAverage(false);
        cout << "Left speaker : " << stats.minValues[0] << " to " << stats.maxValues[0]
             << ", interval=" << stats.getInterval()
             << ", avg=" << avgRMS << endl;

        avgRMS = stats.getAverage(true);
        cout << "Right speaker: " << stats.minValues[1] << " to " << stats.maxValues[1]
             << ", interval=" << stats.getInterval(true)
             << ", avg=" << avgRMS << endl;
}
}


void
RendererTest::expectAverageRMS(const Stats &stats, float expectedAverageRMS)
{
    if (stats.numValues == 0)
        assert(!"No measurements taken");
    else
    {
        float avgRMS = stats.getAverage();

        float diff = fabsf(avgRMS - expectedAverageRMS);
        const float tolerance = 0.002f;
        cout << "Expected average RMS: " << expectedAverageRMS
             << "; diff: " << diff
             << "; tolerance: " << tolerance
             << endl;

        assert(diff <= tolerance);
    }
}


// Expects 'stats' to contain at least one measurement and that these measurements
// all be between 'minRMS' and 'maxRMS'.
//
void
RendererTest::expectRMSRange(const Stats &stats, float minRMS, float maxRMS, bool rightSpeaker /*= false*/)
{
    if (stats.numValues == 0)
        assert(!"No measurements taken");
    else
    {
        cout << "Expected RMS range at " << (rightSpeaker ? "right" : "left ")
             << ": [" << minRMS << ", " << maxRMS << "]" << endl;
        size_t index = (rightSpeaker ? 1 : 0);
        assert(minRMS <= stats.minValues[index]);
        assert(stats.maxValues[index] <= maxRMS);
    }
}

void  //static
RendererTest::onChildRemoved(FrameSource *child, void *userData, FrameSourceAdder * /*adder*/)
{
    //cout << "- RendererTest::onChildRemoved(" << child << ")" << endl;
    RendererTest *test = (RendererTest *) userData;
    AutoLocker locker(test->mutex);
    test->removedChildren.insert(child);  // this pointer will not be dereferenced
}


void
RendererTest::checkNoChildrenInRemovedList()
{
    AutoLocker locker(mutex);
    assert(removedChildren.size() == 0);
}


float
RendererTest::playPanner(const char *desc,
                         SampleToFramePanner *panner,
                         Stats *stats,
                         float maxPlayTimeInSeconds /*= 0*/)
{
    cout << endl;
    cout << "Playing: " << desc << endl;

    checkNoChildrenInRemovedList();

    Uint32 start = SDL_GetTicks();
    //cout << "- RendererTest::playPanner: adding child " << panner << " at time " << start << endl;

    preMeterAdder->addChild(panner);

    waitForChildRemoval(panner, stats, maxPlayTimeInSeconds);
    Uint32 elapsedMS = SDL_GetTicks() - start;

    float elapsedSeconds = elapsedMS / 1000.0f;
    cout << "Elapsed time: " << elapsedSeconds << " s" << endl;

    SDL_Delay(100);
    checkNoChildrenInRemovedList();
    return elapsedSeconds;
}


float
RendererTest::playSampleSource(const char *desc,
                               SampleSource *ss,
                               Stats *stats,
                               float maxPlayTimeInSeconds /*= 0*/)
{
    SampleToFramePanner *panner = new SampleToFramePanner(ss, NULL);
    return playPanner(desc, panner, stats, maxPlayTimeInSeconds);
}


void
RendererTest::playSampleSource(const char *desc,
                               SampleSource *ss,
                               float expectedDuration,
                               float delayBeforeRMSMeasurement,
                               float expectedLeftRMS,
                               float expectedRightRMS)
{
    const float durationTimeout = 1;  // seconds
    const float durationTolerance = 0.145f;  // seconds
    const float rmsTolerance = 0.010f;   // linear volume

    cout << endl;
    cout << "Playing: " << desc << endl;
    checkNoChildrenInRemovedList();

    // Start the sound.
    //
    SampleToFramePanner *panner = new SampleToFramePanner(ss, NULL);
    Uint32 start = SDL_GetTicks();
    preMeterAdder->addChild(panner);

    float elapsedSeconds = 0;
    bool rmsMeasured = false;
    float leftLinVol = 0, rightLinVol = 0;
    for (;;)
    {
        SDL_Delay(16);  // simulate about 60 fps

        elapsedSeconds = (SDL_GetTicks() - start) / 1000.0f;
        if (! rmsMeasured && elapsedSeconds >= delayBeforeRMSMeasurement)
        {
            volumeMeter->getCurrentRMSVolumes(leftLinVol, rightLinVol);
            cout << "RMS after " << elapsedSeconds
                 << " s: L=" << leftLinVol
                 << ", R=" << rightLinVol << endl;
            rmsMeasured = true;
        }

        if (elapsedSeconds >= expectedDuration + durationTimeout)
        {
            cout << "ERROR: timeout at " << durationTimeout
                 << " s past expected duration of " << expectedDuration << " s." << endl;
            assert(!"timeout");
            return;
        }

        if (hasChildBeenRemoved(panner))
            break;
    }

    float durationDiff = fabsf(elapsedSeconds - expectedDuration);

    cout << "Elapsed time : " << elapsedSeconds << " s" << endl;
    cout << "Expected time: " << expectedDuration << " s" << endl;
    cout << "Difference   : " << durationDiff <<  " s (tolerance = "
                              << durationTolerance << ")" << endl;

    float leftRMSDiff  = fabsf(leftLinVol  - expectedLeftRMS);
    float rightRMSDiff = fabsf(rightLinVol - expectedRightRMS);

    cout << "Measured RMS : L=" << leftLinVol << ", R=" << rightLinVol << endl;
    cout << "Expected RMS : L=" << expectedLeftRMS << ", R=" << expectedRightRMS << endl;
    cout << "Differences  : L=" << leftRMSDiff << ", R=" << rightRMSDiff
                                << " (tolerance = " << rmsTolerance << ")" << endl;

    assert(durationDiff <= durationTolerance);
    assert(leftRMSDiff <= rmsTolerance);
    assert(rightRMSDiff <= rmsTolerance);

    checkNoChildrenInRemovedList();
    SDL_Delay(100);
}


void
RendererTest::playSampleSourcePair(const char *desc,
                                  SampleSource *ss0, SampleSource *ss1,
                                  Stats *stats)
{
    cout << endl;
    cout << "Playing: " << desc << endl;

    checkNoChildrenInRemovedList();
    SampleToFramePanner *child0 = new SampleToFramePanner(ss0, NULL);
    SampleToFramePanner *child1 = new SampleToFramePanner(ss1, NULL);

    Uint32 start = SDL_GetTicks();

    preMeterAdder->addChild(child0);
    preMeterAdder->addChild(child1);

    waitForChildRemoval(child0, stats);
    waitForChildRemoval(child1, NULL);
    Uint32 elapsedMS = SDL_GetTicks() - start;

    cout << "Elapsed time: " << elapsedMS / 1000.0f << " s" << endl;

    SDL_Delay(100);
    checkNoChildrenInRemovedList();
}


// Resets 'stats' and adds measurements to it.
//
void
RendererTest::waitForChildRemoval(FrameSource *child,
                                 Stats *stats,
                                 float maxPlayTimeInSeconds /*= 0*/)
{
    //cout << "- RendererTest::waitForChildRemoval(" << child << "): start" << endl;

    if (stats != NULL)
        stats->reset();

    enum { TIMEOUT_IN_SECONDS = 10, STEP_IN_MS = 100 };

    assert(maxPlayTimeInSeconds < TIMEOUT_IN_SECONDS);
    Uint32 startTimeMS = SDL_GetTicks();

    for (size_t iter = 1; iter <= TIMEOUT_IN_SECONDS * 1000 / STEP_IN_MS; ++iter)
    {
        SDL_Delay(STEP_IN_MS);

        if (stats != NULL && iter >= 2 && iter <= 16)  // wait before starting measurements, to let RMS stabilize
        {
            float leftLinVol, rightLinVol;
            volumeMeter->getCurrentRMSVolumes(leftLinVol, rightLinVol);
            //cout << "[" << leftLinVol << ", " << rightLinVol << "]" << endl;

            stats->add(leftLinVol, rightLinVol);
        }

        float elapsedSeconds = (SDL_GetTicks() - startTimeMS) / 1000.0f;
        if (maxPlayTimeInSeconds > 0 && elapsedSeconds >= maxPlayTimeInSeconds)
        {
            cout << "Killing child after " << elapsedSeconds << endl;
            if (! preMeterAdder->killChild(child))
                assert("killChild failed");
            maxPlayTimeInSeconds = 0;  // wait indefinitely for end notification
        }

        if (hasChildBeenRemoved(child))
            return;
    }

    assert(!"timeout");

    //cout << "- RendererTest::waitForChildRemoval(" << child << "): end" << endl;
}


bool
RendererTest::hasChildBeenRemoved(FrameSource *child)
{
    assert(child != NULL);

    AutoLocker locker(mutex);
    if (removedChildren.find(child) != removedChildren.end())
    {
        //cout << "- RendererTest::hasChildBeenRemoved(" << child << "): child removed" << endl;
        removedChildren.erase(child);
        return true;
    }
    return false;
}


float
RendererTest::getLeftRMS() const
{
    float leftLinVol, rightLinVol;
    volumeMeter->getCurrentRMSVolumes(leftLinVol, rightLinVol);
    return leftLinVol;
}


void
RendererTest::measureRMS(Stats &stats, float durationInSeconds)
{
    SDL_Delay(200);  // RMS stabilization delay

    stats.reset();
    Uint32 start = SDL_GetTicks();
    while (SDL_GetTicks() - start < durationInSeconds * 1000)
    {
        SDL_Delay(100);
        float leftLinVol, rightLinVol;
        volumeMeter->getCurrentRMSVolumes(leftLinVol, rightLinVol);
        stats.add(leftLinVol, rightLinVol);
    }
}
