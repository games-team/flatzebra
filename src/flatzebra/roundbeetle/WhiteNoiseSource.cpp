/*  WhiteNoiseSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/WhiteNoiseSource.h>

#include <assert.h>
#include <math.h>

using namespace roundbeetle;


static size_t
getNumSamples(float durationInSeconds)
{
    if (durationInSeconds == 0)
        return size_t(-1);
    double numSamples = double(durationInSeconds) * double(SoundRenderer::freq());
    return numSamples <= size_t(-1) ? size_t(numSamples) : size_t(-1);
}


WhiteNoiseSource::WhiteNoiseSource(float _linVol, float _durationInSeconds)
:   sampleCounter(0),
    maxNumSamples(getNumSamples(_durationInSeconds)),
    minSampleValue(int(-32767 * _linVol)),
    intervalSize(minSampleValue * -2 + 1)
{
    assert(_linVol >= 0 && _linVol <= 1);
}


size_t  //virtual
WhiteNoiseSource::getSamples(Sint16 *dest, size_t numRequested)
{
    if (dest == NULL)
        return 0;

    size_t numSamplesLeft = maxNumSamples - sampleCounter;
    size_t numDelivered = std::min(numRequested, numSamplesLeft);
    for (size_t i = 0; i < numDelivered; ++i, ++sampleCounter)
        dest[i] = Sint16(rand() % intervalSize + minSampleValue);

    return numDelivered;
}


bool  //virtual
WhiteNoiseSource::isFinished() const
{
    return sampleCounter >= maxNumSamples;
}


bool  //virtual
WhiteNoiseSource::rewind()
{
    sampleCounter = 0;
    return true;
}
