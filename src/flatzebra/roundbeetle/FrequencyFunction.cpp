/*  FrequencyFunction.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/FrequencyFunction.h>

#include <roundbeetle/SoundRenderer.h>

#include <assert.h>

using namespace roundbeetle;


LinearMovingFreq::LinearMovingFreq(float _startFreqInHz, float _endFreqInHz, float _totalTimeInS)
:   startFreq(_startFreqInHz),
    rendererFreq(SoundRenderer::freq()),
    factor((_endFreqInHz - _startFreqInHz) / (rendererFreq * _totalTimeInS))
{
    assert(_startFreqInHz > 0);
    assert(_endFreqInHz > 0);
    assert(_totalTimeInS > 0);
    assert(rendererFreq > 0);
}


//virtual
LinearMovingFreq::~LinearMovingFreq()
{
}


size_t  //virtual
LinearMovingFreq::getNumSamplesPerPeriod(size_t sampleCounter)
{
    float freq = startFreq + sampleCounter * factor;
    assert(freq > 0);
    return size_t(rendererFreq / freq);
}
