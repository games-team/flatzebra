/*  SoundEngine.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/SoundEngine.h>

#include <roundbeetle/NativeSDLSoundRenderer.h>
#include <roundbeetle/SoundRenderer.h>
#include <roundbeetle/FrameSource.h>
#include <roundbeetle/FrameSourceAdder.h>
#include <roundbeetle/WaveFileSource.h>
#include <roundbeetle/SampleToFramePanner.h>
#include <roundbeetle/SineSource.h>
#include <roundbeetle/SquareWaveSource.h>
#include <roundbeetle/WhiteNoiseSource.h>
#include <roundbeetle/PausableSource.h>
#include <roundbeetle/ADSRSource.h>
#include <roundbeetle/ADSR.h>
#include <roundbeetle/LoopingSource.h>

#include <iostream>


namespace roundbeetle {

using namespace std;


SoundEngine *SoundEngine::inst = NULL;


SoundEngine::SoundEngine(float _mainBusVolumeMeterWindowLengthInSeconds)
:   mainBus(true, _mainBusVolumeMeterWindowLengthInSeconds),
    reqTable(),
    reqHandleGenerator(0),
    pauseCounter(0),
    onRequestFinishedCallback(NULL),
    onRequestFinishedUserData(NULL)
{
}


int
SoundEngine::create(int rendererFreq,
               float mainBusVolumeMeterWindowLengthInSeconds,
               const char *rawRecordingFilename)
{
    assert(inst == NULL);

    NativeSDLSoundRenderer &soundRenderer = NativeSDLSoundRenderer::create(rendererFreq);

    if (rawRecordingFilename != NULL)
    {
        soundRenderer.openRecordingFile(rawRecordingFilename);
        soundRenderer.startRecording();
    }

    int status = soundRenderer.start();
    if (status != 0)
    {
        NativeSDLSoundRenderer::destroy();
        return status;
    }

    inst = new SoundEngine(mainBusVolumeMeterWindowLengthInSeconds);
    return 0;  // success
}


SoundEngine &
SoundEngine::instance()
{
    assert(inst != NULL);
    return *inst;
}


void
SoundEngine::destroy()
{
    if (inst == NULL)
        return;

    if (SoundRenderer::hasInstance())
    {
        NativeSDLSoundRenderer *soundRenderer = dynamic_cast<NativeSDLSoundRenderer *>(&SoundRenderer::instance());  // TODO: Avoid downcast.
        if (soundRenderer && soundRenderer->stopRecording())
        {
            if (! soundRenderer->closeRecordingFile())
                cerr << "SoundEngine::destroy: failed to close recording file" << endl;
        }
    }

    delete inst;  // ~SoundEngine() calls getTopFrameSourceAdder() on SoundRenderer
    inst = NULL;

    if (SoundRenderer::hasInstance())
    {
        NativeSDLSoundRenderer *soundRenderer = dynamic_cast<NativeSDLSoundRenderer *>(&SoundRenderer::instance());  // TODO: Avoid downcast.
        soundRenderer->requestStop();
        soundRenderer->waitForEnd();
        NativeSDLSoundRenderer::destroy();
    }
}


void
SoundEngine::postBusInit()
{
    if (SoundRenderer::hasInstance())
        SoundRenderer::instance().getTopFrameSourceAdder().addChild(&mainBus.getTopFrameSource());

    // Get notified when a child of the main bus finishes.
    //
    mainBus.getAdder().registerChildRemovedCallback(onChildRemovedStatic, this);
}


SoundEngine::~SoundEngine()
{
    if (SoundRenderer::hasInstance()
            && ! SoundRenderer::instance().getTopFrameSourceAdder().killChild(&mainBus.getTopFrameSource()))
        assert(!"killChild() failed on mainBus top frame source");

    // mainBus's top frame source and its children have now been
    // destroyed by killChild().
    // Tell mainBus to reset its top frame source pointer, so that
    // ~Bus() does not have anything to destroy (it is already done).
    //
    mainBus.reset();
}


// FrameSourceAdder::removeChild() invokes this callback under the renderer mutex.
// This callback must be as quick as possible.
//
void  //static
SoundEngine::onChildRemovedStatic(FrameSource *child, void *userData, FrameSourceAdder *adder)
{
    assert(child != NULL);
    assert(userData != NULL);
    assert(adder != NULL);

    reinterpret_cast<SoundEngine *>(userData)->onChildRemoved(child, adder);
}


void
SoundEngine::onChildRemoved(FrameSource *child, FrameSourceAdder * /*adder*/)
{
    //cout << "onChildRemoved(" << child << ", _)" << endl;
    RequestMap::iterator it = findReqHandle(child);
    assert(it != reqTable.end());
    if (onRequestFinishedCallback != NULL)
        (*onRequestFinishedCallback)(it->first, onRequestFinishedUserData);
    reqTable.erase(it);
}


void
SoundEngine::registerRequestFinishedCallback(RequestFinishedCallback cb, void *userData)
{
    AutoLocker a(SoundRenderer::getMutex());

    onRequestFinishedCallback = cb;
    onRequestFinishedUserData = userData;
}


Bus &
SoundEngine::getMainBus()
{
    return mainBus;
}


bool
SoundEngine::isPostBusInitDone() const
{
    FrameSourceAdder::OnChildRemovedCallback cb = NULL;
    void *userData = NULL;

    {
        AutoLocker a(SoundRenderer::getMutex());

        mainBus.getAdder().getChildRemovedCallback(cb, userData);
    }

    assert(cb == NULL || cb == onChildRemovedStatic);
    assert(userData == NULL || userData == this);
    assert((cb == NULL) == (userData == NULL));
    return cb == &SoundEngine::onChildRemovedStatic;
}


int
SoundEngine::requestWaveFileSound(const WaveFileBuffer &wfb, Bus &bus)
{
    if (! isPostBusInitDone())  // each request*() method must to this check
        return -1;
    WaveFileSource *wfs = new WaveFileSource(wfb);
    return addSampleSourceToBus(wfs, NULL, 1, bus);
}


int
SoundEngine::requestSine(float _toneFreq, float _linVol, float _durationInSeconds, Bus &bus)
{
    if (! isPostBusInitDone())
        return -1;
    SineSource *ss = new SineSource(_toneFreq, _linVol, _durationInSeconds);
    return addSampleSourceToBus(ss, NULL, 1, bus);
}


int
SoundEngine::requestSquareWave(float _freq, const ADSR &adsr, size_t loopCount, Bus &bus)
{
    if (! isPostBusInitDone())
        return -1;
    SquareWaveSource *ss = new SquareWaveSource(_freq, 1, 0);  // infinite
    return addSampleSourceToBus(ss, &adsr, loopCount, bus);
}


int
SoundEngine::requestSquareWave(FrequencyFunction &freqFunc, const ADSR &adsr, size_t loopCount, Bus &bus)
{
    if (! isPostBusInitDone())
        return -1;
    SquareWaveSource *ss = new SquareWaveSource(freqFunc, 1, 0);  // infinite
    return addSampleSourceToBus(ss, &adsr, loopCount, bus);
}


bool
SoundEngine::setSquareWaveFreq(int reqHandle, float _newFreq)
{
    AutoLocker a(SoundRenderer::getMutex());

    RequestMap::const_iterator it = reqTable.find(reqHandle);
    //cout << "setSquareWaveFreq(" << reqHandle << "): " << it->first << ", " << it->second << endl;
    if (it == reqTable.end())  // if request handle not found
        return false;  // request is dead

    SampleSource *ss = it->second.clientSampleSource;
    assert(ss != NULL);
    SquareWaveSource *svs = dynamic_cast<SquareWaveSource *>(ss);
    if (svs == NULL)
        return false;  // request is not a square wave

    svs->setFrequency(_newFreq);
    return true;  // success
}


int
SoundEngine::requestWhiteNoise(const ADSR &adsr, size_t loopCount, Bus &bus)
{
    if (! isPostBusInitDone())
        return -1;
    WhiteNoiseSource *ss = new WhiteNoiseSource(1, 0);  // infinite
    return addSampleSourceToBus(ss, &adsr, loopCount, bus);
}


// ss: must come from new; becomes owned by bus adder.
// Increments reqHandleGenerator.
// Locks renderer mutex.
//
// Path:
// PausableSource <- SampleToFramePanner <- LoopingSource? <- ADSRSource? <- SampleSource
// ? = optional
//
int
SoundEngine::addSampleSourceToBus(SampleSource *ss, const ADSR *adsr, size_t loopCount, Bus &bus)
{
    ADSRSource *as = (adsr == NULL ? NULL : new ADSRSource(ss, *adsr));

    SampleSource *loopableSource = (as == NULL ? ss : as);

    // Only insert a LoopingSource if necessary:
    SampleSource *ls = (loopCount == 1
                        ? loopableSource
                        : new LoopingSource(loopableSource, loopCount));
    /*cout << "Engine::addSampleSourceToBus: ss=" << ss
         << ", as=" << as
         << ", loopableSource=" << loopableSource
         << ", ls=" << ls
         << endl;*/

    SampleToFramePanner *panner = new SampleToFramePanner(ls, NULL);
    PausableSource *ps = new PausableSource(panner);

    int reqHandle;
    {
        AutoLocker a(SoundRenderer::getMutex());

        reqHandle = reqHandleGenerator++;
        reqTable[reqHandle] = Desc(ss, as, ps, &bus);
        bus.getAdder().addChildUnsafe(ps);
    }

    return reqHandle;
}


bool
SoundEngine::isRequestAlive(int reqHandle) const
{
    AutoLocker a(SoundRenderer::getMutex());

    RequestMap::const_iterator it = reqTable.find(reqHandle);
    //cout << "isRequestAlive(" << reqHandle << "): " << it->first << ", " << it->second << endl;
    return it != reqTable.end();
}


// Must be called under protection of renderer mutex.
//
PausableSource *
SoundEngine::getPausableSourceFromRequestHandle(int reqHandle)
{
    RequestMap::const_iterator it = reqTable.find(reqHandle);
    if (it == reqTable.end())
        return NULL;  // request already finished, or inexistent

    FrameSource *fs = it->second.frameSource;
    assert(fs != NULL);
    PausableSource *ps = dynamic_cast<PausableSource *>(fs);
    return ps;  // null if request is not pausable
}


bool
SoundEngine::pauseRequest(int reqHandle)
{
    AutoLocker a(SoundRenderer::getMutex());

    PausableSource *ps = getPausableSourceFromRequestHandle(reqHandle);
    if (ps == NULL)
        return false;
    ps->pauseUnsafe();  // unsafe is fine because we have already locked renderer mutex
    return true;
}


bool
SoundEngine::resumeRequest(int reqHandle)
{
    AutoLocker a(SoundRenderer::getMutex());

    PausableSource *ps = getPausableSourceFromRequestHandle(reqHandle);
    if (ps == NULL)
        return false;
    ps->resumeUnsafe();  // unsafe is fine because we have already locked renderer mutex
    return true;
}


bool
SoundEngine::stopRequest(int reqHandle)
{
    AutoLocker a(SoundRenderer::getMutex());

    RequestMap::const_iterator it = reqTable.find(reqHandle);
    if (it == reqTable.end())
        return false;  // request already finished, or inexistent

    Bus *bus = it->second.bus;
    assert(bus != NULL);
    FrameSourceAdder &adder = bus->getAdder();

    FrameSource *adderChild = it->second.frameSource;
    assert(adderChild != NULL);
    return adder.killChildUnsafe(adderChild);
}


bool
SoundEngine::pauseEngine()
{
    AutoLocker a(SoundRenderer::getMutex());
    ++pauseCounter;
    if (pauseCounter == 1)
        SDL_PauseAudio(1);  // stop callback invocation
    return true;
}


bool
SoundEngine::resumeEngine()
{
    AutoLocker a(SoundRenderer::getMutex());
    if (pauseCounter == 0)  // if already running
        return true;
    --pauseCounter;
    if (pauseCounter > 0)  // if still paused
        return false;
    SDL_PauseAudio(0);  // resume callback invocation
    return true;
}


SoundEngine::RequestMap::iterator
SoundEngine::findReqHandle(FrameSource *child)
{
    for (RequestMap::iterator it = reqTable.begin();
                             it != reqTable.end(); ++it)
        if (it->second.frameSource == child)
            return it;

    return reqTable.end();
}


}  // namespace roundbeetle
