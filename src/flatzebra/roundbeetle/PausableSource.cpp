/*  PausableSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/PausableSource.h>

#include <roundbeetle/AutoLocker.h>
#include <roundbeetle/SoundRenderer.h>

#include <assert.h>
#include <math.h>
#include <iostream>

using namespace roundbeetle;


PausableSource::PausableSource(FrameSource *_src)
:   FrameSource(),
    src(_src),
    pauseCounter(0)
{
}


//virtual
PausableSource::~PausableSource()
{
    delete src;
}


void
PausableSource::pause()
{
    AutoLocker a(SoundRenderer::getMutex());
    pauseUnsafe();
}


void
PausableSource::pauseUnsafe()
{
    ++pauseCounter;
    //std::cout << "PS{" << this << "}::P: " << pauseCounter << std::endl;
}


void
PausableSource::resume()
{
    AutoLocker a(SoundRenderer::getMutex());
    resumeUnsafe();
}


void
PausableSource::resumeUnsafe()
{
    if (pauseCounter > 0)
        --pauseCounter;
    //std::cout << "PS{" << this << "}::R: " << pauseCounter << std::endl;
}


size_t  //virtual
PausableSource::getFrames(Frame *frameBuffer, size_t numFramesToFill)
{
    //std::cout << "PausableSource(" << this << ")::getFrames(): pauseCounter=" << pauseCounter << std::endl;
    if (frameBuffer == NULL)
        return 0;

    if (pauseCounter == 0)  // if not paused
        return src->getFrames(frameBuffer, numFramesToFill);

    // During pause, return silence.
    memset(frameBuffer, '\0', sizeof(frameBuffer[0]) * numFramesToFill);
    return numFramesToFill;
}


bool  //virtual
PausableSource::isFinished() const
{
    return src->isFinished();
}


bool  //virtual
PausableSource::rewind()
{
    return src->rewind();
}
