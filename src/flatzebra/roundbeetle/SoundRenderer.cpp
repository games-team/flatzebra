/*  SoundRenderer.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/SoundRenderer.h>

#include <assert.h>


using namespace roundbeetle;


SoundRenderer *SoundRenderer::inst = NULL;


SoundRenderer::SoundRenderer(float _rendererFreqInHz)
:   mutex(SDL_CreateMutex()),
    rendererFreqInHz(_rendererFreqInHz),
    topFrameSource(1),
    recording(false)
{
}


SoundRenderer::~SoundRenderer()
{
    SDL_DestroyMutex(mutex);
}


bool
SoundRenderer::hasInstance()
{
    return inst != NULL;
}


SoundRenderer &
SoundRenderer::instance()
{
    assert(inst != NULL);
    return *inst;
}


SDL_mutex *
SoundRenderer::getMutex()
{
    assert(inst != NULL);
    return inst->mutex;
}


int
SoundRenderer::start()
{
    if (mutex == NULL)
        return -1;

    return initAudio();  // virtual
}


float
SoundRenderer::getRendererFreq() const
{
    return rendererFreqInHz;
}


float  // static
SoundRenderer::freq()
{
    return inst != NULL ? inst->getRendererFreq() : 1;
}


FrameSourceAdder &
SoundRenderer::getTopFrameSourceAdder()
{
    return topFrameSource;
}


void
SoundRenderer::requestStop()
{
}


void
SoundRenderer::waitForEnd()
{
}


void
SoundRenderer::startRecording()
{
    AutoLocker a(mutex);
    recording = true;
}


bool
SoundRenderer::isRecording() const
{
    AutoLocker a(mutex);
    return recording;
}


bool
SoundRenderer::stopRecording()
{
    AutoLocker a(mutex);
    bool wasRecording = recording;
    recording = false;
    return wasRecording;
}


bool
SoundRenderer::isBusy() const
{
    AutoLocker a(mutex);
    return topFrameSource.isBusy();
}


void
SoundRenderer::getFramesFromTopSource(Frame *frameBuffer, size_t numFramesToFill)
{
    AutoLocker a(mutex);
    topFrameSource.getFrames(frameBuffer, numFramesToFill);

    if (recording)
        record(frameBuffer, numFramesToFill);
}


void
SoundRenderer::record(Frame * /*frameBuffer*/, size_t /*numFramesToFill*/)
{
}
