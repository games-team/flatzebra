/*  LoopingSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/LoopingSource.h>


namespace roundbeetle {


LoopingSource::LoopingSource(SampleSource *_src, size_t _numLoops /*= 0*/)
:   SampleSource(),
    src(_src),
    numLoops(_numLoops),
    loopCounter(0)
{
    //std::cout << "LoopingSource::LoopingSource(src=" << _src << ", " << _numLoops << ")" << std::endl;
    assert(_src != NULL);
}


//virtual
LoopingSource::~LoopingSource()
{
    delete src;
}


size_t  //virtual
LoopingSource::getSamples(Sint16 *dest, size_t numRequested)
{
    /*std::cout << "LoopingSource::getSamples(" << dest << ", " << numRequested
              << "): numLoops=" << numLoops
              << ", loopCounter=" << loopCounter
              << ", src=" << src << std::endl;*/
    if (dest == NULL || numRequested == 0 || src == NULL)
        return 0;

    size_t numReturned = 0;

    for (;;)
    {
        size_t numObtained = src->getSamples(dest, numRequested - numReturned);
        assert(numObtained <= numRequested - numReturned);
        numReturned += numObtained;
        /*std::cout << "LoopingSource::getSamples:   asked " << numRequested - numReturned
                  << " from " << src << ", got " << numObtained
                  << "; numReturned now at " << numReturned
                  << ", numRequested=" << numRequested
                  << ", src " << (src->isFinished() ? "FINISHED" : "not finished")
                  << std::endl;*/
        if (numReturned == numRequested)
            return numRequested;
        dest += numObtained;

        if (src->isFinished())
        {
            ++loopCounter;
            //std::cout << "LoopingSource::getSamples:   loopCounter=" << loopCounter << std::endl;

            if (isFinished())  // if finished number of loops to do
                return numReturned;  // stop

            if (!src->rewind())  // if source finished but rewinding it fails
                return numReturned;
        }
    }
}


bool  //virtual
LoopingSource::isFinished() const
{
    // Number of loops to do is finite, and reached:
    return numLoops > 0 && loopCounter >= numLoops;
}


bool  //virtual
LoopingSource::rewind()
{
    loopCounter = 0;
    if (src == NULL)
        return false;
    return src->rewind();
}


}  // namespace roundbeetle
