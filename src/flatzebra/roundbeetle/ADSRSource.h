/*  ADSRSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/SampleSource.h>

#include <assert.h>
#include <math.h>
#include <iostream>


namespace roundbeetle {

class ADSR;


class ADSRSource : public SampleSource
{
public:

    // _adsr: the total time in seconds multiplied by the renderer frequency
    //        in Hertz must not exceed 2**32 - 1, i.e., 0xffffffff.
    //
    ADSRSource(SampleSource *_src, const ADSR &_adsr);

    virtual ~ADSRSource();

    // Returns mono PCM-16 signal.
    // Returns no samples after the release time has passed.
    //
    virtual size_t getSamples(Sint16 *dest, size_t numRequested) override;

    // Return true if the source has finished or if the release time has passed.
    //
    virtual bool isFinished() const override;

    virtual bool rewind() override;

private:

    float getCurrentLevel() const;

    ADSRSource(const ADSRSource &);
    ADSRSource &operator = (const ADSRSource &);

private:

    SampleSource *src;
    size_t idx;
    const float attackLevel;
    const float sustainLevel;
    const size_t decayStartIdx;
    const size_t sustainStartIdx;
    const size_t releaseStartIdx;
    const size_t endIdx;

};


}  // namespace roundbeetle
