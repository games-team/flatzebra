/*  NativeSDLSoundRenderer.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/SoundRenderer.h>

#include <SDL2/SDL.h>

#include <sys/time.h>
#include <iostream>
#include <fstream>

//#define MEASURE_SOUND_RENDERER_PERFORMANCE


namespace roundbeetle {


class NativeSDLSoundRenderer : public SoundRenderer
{
protected:

    NativeSDLSoundRenderer(int _rendererFreqInHz);

public:

    static NativeSDLSoundRenderer &create(int _rendererFreqInHz);

    static void destroy();

    virtual ~NativeSDLSoundRenderer();

    // Inherited.
    //
    virtual void requestStop() override;

    bool openRecordingFile(const std::string &filename);

    bool closeRecordingFile();

protected:

    // Inherited.
    //
    virtual int initAudio() override;

    static void fillerCallback(void *userdata, Uint8 *stream, int len);

    void fill(Uint8 *stream, int len);

    // Called with every framebuffer obtained from the top frame source.
    //
    virtual void record(Frame *frameBuffer, size_t numFramesToFill) override;

protected:

    SDL_AudioSpec desired;
    SDL_AudioSpec obtained;

    size_t numCallbackInvocations;
    double accWallTime;
    std::ofstream recordingFile;

};


}  // namespace roundbeetle
