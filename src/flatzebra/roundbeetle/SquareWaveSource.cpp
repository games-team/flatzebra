/*  SquareWaveSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/SquareWaveSource.h>
#include <roundbeetle/FrequencyFunction.h>


using namespace roundbeetle;


static size_t
getNumSamples(float durationInSeconds)
{
    if (durationInSeconds == 0)
        return size_t(-1);
    double numSamples = double(durationInSeconds) * double(SoundRenderer::freq());
    return numSamples <= size_t(-1) ? size_t(numSamples) : size_t(-1);
}


SquareWaveSource::SquareWaveSource(float _freq, float _linVol, float _durationInSeconds)
:   sampleCounter(0),
    samplesInPeriodCounter(0),
    maxNumSamples(getNumSamples(_durationInSeconds)),
    samplesPerPeriod(0),
    frequencyFunction(NULL),
    pendingSamplesPerPeriod(0),
    sampleValue(Sint16(_linVol * 32767))
{
    assert(_linVol >= 0 && _linVol <= 1);
    assert(_durationInSeconds >= 0);
    assert(_durationInSeconds < float(size_t(-1)) / SoundRenderer::freq());  // max allowed duration

    setFrequency(_freq);  // initializes pendingSamplesPerPeriod
    samplesPerPeriod = pendingSamplesPerPeriod;
    assert(samplesPerPeriod > 0);
}


SquareWaveSource::SquareWaveSource(FrequencyFunction &_frequencyFunction, float _linVol, float _durationInSeconds)
:   sampleCounter(0),
    samplesInPeriodCounter(0),
    maxNumSamples(getNumSamples(_durationInSeconds)),
    samplesPerPeriod(0),
    frequencyFunction(&_frequencyFunction),
    pendingSamplesPerPeriod(0),
    sampleValue(Sint16(_linVol * 32767))
{
    assert(_linVol >= 0 && _linVol <= 1);
    assert(_durationInSeconds >= 0);
    assert(_durationInSeconds < float(size_t(-1)) / SoundRenderer::freq());  // max allowed duration

    samplesPerPeriod = frequencyFunction->getNumSamplesPerPeriod(0);
    assert(samplesPerPeriod > 0);
}


void
SquareWaveSource::setFrequency(float newFreq)
{
    if (newFreq < 20 || newFreq > 20000)
        newFreq = 20;

    size_t s = size_t(SoundRenderer::freq() / newFreq);
    assert(s > 0);

    AutoLocker a(SoundRenderer::getMutex());  // lock mutex under which getSamples() is called
    pendingSamplesPerPeriod = s;
    frequencyFunction = NULL;
}


size_t  //virtual
SquareWaveSource::getSamples(Sint16 *dest, size_t numRequested)
{
    /*std::cout << "SquareWaveSource::getSamples(" << dest << ", " << numRequested
              << "): maxNumSamples=" << maxNumSamples
              << ", sampleCounter=" << sampleCounter
              << std::endl;*/

    if (dest == NULL)
        return 0;

    size_t numSamplesLeft = maxNumSamples - sampleCounter;
    size_t numDelivered = std::min(numRequested, numSamplesLeft);
    for (size_t i = 0; i < numDelivered; ++i, ++sampleCounter)
    {
        dest[i] = (samplesInPeriodCounter < samplesPerPeriod / 2 ? + sampleValue : - sampleValue);

        ++samplesInPeriodCounter;
        if (samplesInPeriodCounter == samplesPerPeriod)  // if complete period done
        {
            if (frequencyFunction != NULL)
                samplesPerPeriod = frequencyFunction->getNumSamplesPerPeriod(sampleCounter);
            else
                samplesPerPeriod = pendingSamplesPerPeriod;  // change to newly requested frequency

            samplesInPeriodCounter = 0;  // start new period
        }
    }

    return numDelivered;
}


bool  //virtual
SquareWaveSource::isFinished() const
{
    return sampleCounter >= maxNumSamples;
}


bool  //virtual
SquareWaveSource::rewind()
{
    sampleCounter = 0;
    return true;
}
