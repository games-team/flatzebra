/*  ADSRSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/ADSRSource.h>

#include <roundbeetle/ADSR.h>


using namespace roundbeetle;


ADSRSource::ADSRSource(SampleSource *_src, const ADSR &_adsr)
:   src(_src),
    idx(0),
    attackLevel(_adsr.attackLevel),
    sustainLevel(_adsr.sustainLevel),
    decayStartIdx(size_t(_adsr.attackTime * SoundRenderer::freq())),
    sustainStartIdx(size_t((_adsr.attackTime + _adsr.decayTime) * SoundRenderer::freq())),
    releaseStartIdx(size_t((_adsr.attackTime + _adsr.decayTime + _adsr.sustainTime) * SoundRenderer::freq())),
    endIdx(size_t((_adsr.attackTime + _adsr.decayTime + _adsr.sustainTime + _adsr.releaseTime) * SoundRenderer::freq()))
{
    assert(_src != NULL);
    /*std::cout << "ADSRSource::ADSRSource: this=" << this
                << ", src=" << src << ", ("
                << attackLevel << ", "
                << sustainLevel << "), ("
                << decayStartIdx << ", "
                << sustainStartIdx << ", "
                << releaseStartIdx << ", "
                << endIdx << ")" << std::endl;*/
}


//virtual
ADSRSource::~ADSRSource()
{
    delete src;
}


size_t  //virtual
ADSRSource::getSamples(Sint16 *dest, size_t numRequested)
{
    /*std::cout << "ADSRSource::getSamples(" << dest << ", " << numRequested
              << "): idx=" << idx << std::endl;*/
    if (idx >= endIdx || numRequested == 0 || dest == NULL || src == NULL)
        return 0;

    size_t numObtained = src->getSamples(dest, numRequested);
    //std::cout << "s->g: " << numObtained << std::endl;
    assert(numObtained <= numRequested);
    for (size_t i = 0; i < numObtained; ++i, ++idx)
    {
        float curLev = getCurrentLevel();  // uses idx
        //std::cout << idx << ". " << curLev << std::endl;
        dest[i] = Sint16(dest[i] * curLev);
    }

    return numObtained;
}


bool  //virtual
ADSRSource::isFinished() const
{
    //std::cout << "ADSRSource::isFinished(): idx=" << idx << ", endIdx=" << endIdx << ", ticks=" << SDL_GetTicks() << std::endl;
    return src->isFinished() || idx >= endIdx;
}


bool  //virtual
ADSRSource::rewind()
{
    src->rewind();
    idx = 0;
    return true;
}


float
ADSRSource::getCurrentLevel() const
{
    float i = float(idx);
    if (idx < decayStartIdx)  // if attack phase
        return (i / decayStartIdx) * attackLevel;

    if (idx < sustainStartIdx)  // if decay phase
        return attackLevel
               + (i - decayStartIdx) / (sustainStartIdx - decayStartIdx)
                 * (sustainLevel - attackLevel);

    if (idx < releaseStartIdx)  // if sustain phase
        return sustainLevel;

    if (idx < endIdx)  // if release phase
        return (1 - (i - releaseStartIdx) / (endIdx - releaseStartIdx)) * sustainLevel;

    return 0;  // silence
}
