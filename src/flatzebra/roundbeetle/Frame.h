/*  Frame.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <SDL2/SDL_stdinc.h>


namespace roundbeetle {


struct Frame
{
    Sint16 left;
    Sint16 right;

    void add(const Frame &f, float factor)
    {
        left  += Sint16(f.left  * factor);
        right += Sint16(f.right * factor);
    }

    void mul(float factor)
    {
        left  = Sint16(left  * factor);
        right = Sint16(right * factor);
    }

};


struct FloatFrame
{
    float left;
    float right;

    void add(const Frame &f)
    {
        left  += f.left;
        right += f.right;
    }

    void convert(Frame &dest) const
    {
        dest.left  = Sint16(left);
        dest.right = Sint16(right);
    }

    void mulAndConvert(Frame &dest, float factor) const
    {
        dest.left  = Sint16(left * factor);
        dest.right = Sint16(right * factor);
    }

};


}  // namespace roundbeetle
