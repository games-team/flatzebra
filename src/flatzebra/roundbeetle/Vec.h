/*  Vec.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <math.h>
#include <iostream>


namespace roundbeetle {


class Vec
{
public:

    Vec(float _x = 0, float _y = 0, float _z = 0)
    :   x(_x),
        y(_y),
        z(_z)
    {
    }

    void set(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    float squaredLength() const
    {
        return x * x + y * y + z * z;
    }

    float length() const
    {
        return sqrtf(squaredLength());
    }

    float x, y, z;
};


inline std::ostream &
operator << (std::ostream &out, const Vec &v)
{
    return out << '(' << v.x << ", " << v.y << ", " << v.z << ')';
}


}  // namespace roundbeetle
