/*  FrameSourceAdder.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/FrameSourceAdder.h>

#include <roundbeetle/SoundRenderer.h>

#include <assert.h>
#include <iostream>
#include <string.h>
#include <algorithm>


using namespace roundbeetle;


//#define DETECT_FRAME_ADDER_OVERFLOWS


FrameSourceAdder::FrameSourceAdder(float _linAttenuation)
:   children(),
    linAttenuation(_linAttenuation),
    addBuffer(),
    numOverflows(0),
    onChildRemoved(NULL),
    userDataOnChildRemoved(NULL)
{
}


//virtual
FrameSourceAdder::~FrameSourceAdder()
{
    for (size_t i = 0; i < children.size(); ++i)
        delete children[i];
}


void
FrameSourceAdder::registerChildRemovedCallback(OnChildRemovedCallback callback, void *userData)
{
    AutoLocker a(SoundRenderer::getMutex());
    onChildRemoved = callback;
    userDataOnChildRemoved = userData;
}


void
FrameSourceAdder::getChildRemovedCallback(OnChildRemovedCallback &cb, void *&userData) const
{
    cb = onChildRemoved;
    userData = userDataOnChildRemoved;
}


void
FrameSourceAdder::setLinearAttenuation(float _linAttenuation)
{
    AutoLocker a(SoundRenderer::getMutex());
    linAttenuation = _linAttenuation;
}


void
FrameSourceAdder::addChild(FrameSource *child)
{
    AutoLocker a(SoundRenderer::getMutex());
    addChildUnsafe(child);
}


void
FrameSourceAdder::addChildUnsafe(FrameSource *child)
{
    if (child != NULL)
    {
        //std::cout << "FrameSourceAdder(" << this << ")::addChildUnsafe: adding child " << child
        //          << " at index " << children.size() << std::endl;
        children.push_back(child);
    }
}


bool
FrameSourceAdder::killChild(FrameSource *child)
{
    AutoLocker a(SoundRenderer::getMutex());
    return killChildUnsafe(child);
}


bool
FrameSourceAdder::killChildUnsafe(FrameSource *child)
{
    if (child == NULL)
        return false;
    std::vector<FrameSource *>::iterator it = std::find(children.begin(), children.end(), child);
    if (it == children.end())
        return false;
    removeChild(it - children.begin());  // invokes callback, deletes child
    return true;
}


bool
FrameSourceAdder::isBusy() const
{
    AutoLocker a(SoundRenderer::getMutex());
    return children.size() != 0;
}


// Current contents of addBuffer[] are lost.
// New contents are initialized to zeroes.
//
void
FrameSourceAdder::resetAddBuffer(size_t newSizeInFrames)
{
    if (newSizeInFrames > addBuffer.size())  // if current buffer too small
    {
        addBuffer.resize(newSizeInFrames);
    }
    memset(&addBuffer.front(), '\0', addBuffer.size() * sizeof(FloatFrame));
}


// getFrames() is called recursively on the children even if linAttenuation
// is zero, because the children must be allowed to advance regardless of
// this adder's volume.
//
// Called by the renderer thread under the protection of SoundRenderer::getMutex().
//
size_t  // virtual
FrameSourceAdder::getFrames(Frame *frameBuffer, size_t numFramesToFill)
{
    // If only one child, avoid addBuffer and additions.
    //
    if (children.size() == 1)
    {
        size_t numFramesObtained = children[0]->getFrames(frameBuffer, numFramesToFill);

        // If child did not fill entire buffer, fill rest with zeroes.
        //
        size_t numMissing = numFramesToFill - numFramesObtained;
        if (numMissing > 0)
            memset(frameBuffer + numFramesObtained, '\0', numMissing * sizeof(Frame));

        // Remove child if finished.
        //
        if (children[0]->isFinished())
            removeChild(0);

        if (linAttenuation != 1)
            for (size_t i = 0; i < numFramesObtained; ++i)
                frameBuffer[i].mul(linAttenuation);

        //std::cout << "FSA: " << numFramesToFill << std::endl;
        return numFramesToFill;
    }


    // If no children, return silence.
    //
    if (children.size() == 0)
    {
        memset(frameBuffer, 0, numFramesToFill * sizeof(Frame));
        return numFramesToFill;
    }


    // More than one child.
    //
    if (children.size() > 0)
    {
        resetAddBuffer(numFramesToFill);  // initialize floats in addBuffer[] to zero

        for (size_t i = 0; i < children.size(); )  // children[] may change size at each iter
        {
            if (children[i] == NULL)
            {
                assert(false);
                continue;
            }

            // Get frames from child into destination buffer (used as temp buffer).
            //
            size_t numFramesObtained = children[i]->getFrames(frameBuffer, numFramesToFill);

            // Add child's 16-bit frames to add buffer's float frames.
            //
            for (size_t f = 0; f < numFramesObtained; ++f)
            {
                #ifdef DETECT_FRAME_ADDER_OVERFLOWS
                // Unsupported code:
                add(frameBuffer[f].left,  childBuffer[f].left, linAttenuation);
                add(frameBuffer[f].right, childBuffer[f].right, linAttenuation);
                #else
                addBuffer[f].add(frameBuffer[f]);
                #endif
            }

            // If this child is finished, remove it from this adder.
            //
            if (children[i]->isFinished())
                removeChild(i);
            else
                ++i;
        }

        // Copy add buffer's floats to 16-bit frames in destination buffer.
        // Apply attenuation if any.
        //
        if (linAttenuation == 1)  // if no attenuation to apply
            for (size_t i = 0; i < numFramesToFill; ++i)
                addBuffer[i].convert(frameBuffer[i]);
        else
            for (size_t i = 0; i < numFramesToFill; ++i)
                addBuffer[i].mulAndConvert(frameBuffer[i], linAttenuation);
    }

    return numFramesToFill;
}


bool
FrameSourceAdder::isFinished() const
{
    return false;
}


bool
FrameSourceAdder::rewind()
{
    return false;  // not supported
}


size_t
FrameSourceAdder::getNumOverflows() const
{
    AutoLocker a(SoundRenderer::getMutex());
    return numOverflows;
}


// To be called under the renderer mutex.
// Invokes the onChildRemoved callback if not null.
//
void
FrameSourceAdder::removeChild(size_t index)
{
    //std::cout << "FrameSourceAdder(" << this << ")::removeChild: removing child " << children[index]
    //           << " from " << index << std::endl;
    assert(index < children.size());
    assert(children[index] != NULL);

    if (onChildRemoved != NULL)
        (*onChildRemoved)(children[index], userDataOnChildRemoved, this);

    delete children[index];

    size_t lastIndex = children.size() - 1;
    if (index < lastIndex)  // if child to remove is not last
        children[index] = children[lastIndex];  // move last child to freed slot
    children.erase(children.begin() + lastIndex);
}


#ifdef DETECT_FRAME_ADDER_OVERFLOWS


void  // static
FrameSourceAdder::add(Sint16 &destSample, Sint16 childSample, float linAttenuation)
{
    Sint32 sum = Sint32(destSample) + Sint32(childSample * linAttenuation);
    destSample = bound(sum);
}


Sint16  // static
FrameSourceAdder::bound(Sint32 sample)
{
    if (sample < -32768)
    {
        //std::cout << sample << std::endl;
        ++numOverflows;
        return -32768;
    }
    if (sample > +32767)
    {
        //std::cout << sample << std::endl;
        ++numOverflows;
        return +32767;
    }
    return Sint16(sample);
}


#endif  // DETECT_FRAME_ADDER_OVERFLOWS
