/*  PausableSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/FrameSource.h>


namespace roundbeetle {


class PausableSource : public FrameSource
{
public:

    // _toneFreq; frequency in Hz of the tone to generate.
    // _linVol: 0..1
    // _rendererFreq: frequency in Hz of the renderer buffer (e.g. 22050 Hz).
    //
    PausableSource(FrameSource *_src);

    virtual ~PausableSource();

    // May be called even if this source is already paused.
    // But then, resume() must be called an equal number of times
    // to let the underlying source play again.
    //
    void pause();

    void pauseUnsafe();

    // Must be called as many times as pause() was called
    // to let the underlying source play again.
    // Does nothing if the underlying source is already playing.
    //
    void resume();

    void resumeUnsafe();

    // Called by renderer thread.
    //
    virtual size_t getFrames(Frame *frameBuffer, size_t numFramesToFill) override;

    virtual bool isFinished() const override;

    virtual bool rewind() override;

private:

    PausableSource(const PausableSource &);
    PausableSource &operator = (const PausableSource &);

private:

    FrameSource *src;
    size_t pauseCounter;

};


}  // namespace roundbeetle
