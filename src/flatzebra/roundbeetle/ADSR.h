/*  ADSR.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once


namespace roundbeetle {


struct ADSR
{
    float attackLevel;   // 0..1
    float sustainLevel;  // 0..1
    float attackTime;   // seconds (>= 0)
    float decayTime;    // seconds (>= 0)
    float sustainTime;  // seconds (>= 0)
    float releaseTime;  // seconds (>= 0)

    ADSR(float _attackLevel,
         float _sustainLevel,
         float _attackTime,
         float _decayTime,
         float _sustainTime,
         float _releaseTime)
    :   attackLevel(_attackLevel),
        sustainLevel(_sustainLevel),
        attackTime(_attackTime),
        decayTime(_decayTime),
        sustainTime(_sustainTime),
        releaseTime(_releaseTime)
    {
    }

    float getTotalTime() const
    {
        return attackTime + decayTime + sustainTime + releaseTime;
    }

};


}  // namespace roundbeetle
