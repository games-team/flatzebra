/*  Bus.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/FrameSourceAdder.h>
#include <flatzebra/roundbeetle/VolumeMeter.h>
#include <flatzebra/roundbeetle/PausableSource.h>


namespace roundbeetle {


class Bus
{
public:

    // _volumeMeterWindowLengthInSeconds: no VolumeMeter added if 0.
    //
    Bus(bool pausable, float _volumeMeterWindowLengthInSeconds)
    :   adder(new FrameSourceAdder(1)),
        pausableSource(NULL),
        volumeMeter(NULL)
    {
        if (pausable)
            pausableSource = new PausableSource(adder);

        if (_volumeMeterWindowLengthInSeconds > 0)
        {
            // VolumeMeter must be parent of pausable source so that bus pauses
            // are perceived by the RMS measurements.
            //
            FrameSource *src = pausableSource != NULL ? (FrameSource *) pausableSource : (FrameSource *) adder;
            volumeMeter = new VolumeMeter(_volumeMeterWindowLengthInSeconds, src);
        }
    }

    ~Bus()
    {
        delete adder;
        delete volumeMeter;
        delete pausableSource;
    }

    void reset()
    {
        adder = NULL;
        volumeMeter = NULL;
        pausableSource = NULL;
    }

    VolumeMeter *getVolumeMeter()
    {
        return volumeMeter;
    }

    FrameSource &getTopFrameSource()
    {
        if (volumeMeter != NULL)
            return *volumeMeter;
        if (pausableSource != NULL)
            return *pausableSource;
        return *adder;
    }

    const FrameSourceAdder &getAdder() const
    {
        return *adder;
    }

    FrameSourceAdder &getAdder()
    {
        return *adder;
    }

    // Locks/unlocks the renderer mutex.
    //
    bool pause()
    {
        //std::cout << "Bus::Pause: pausing " << pausableSource << std::endl;
        if (pausableSource == NULL)
            return false;
        pausableSource->pause();
        return true;
    }

    // Locks/unlocks the renderer mutex.
    //
    bool resume()
    {
        //std::cout << "Bus::Pause: resuming " << pausableSource << std::endl;
        if (pausableSource == NULL)
            return false;
        pausableSource->resume();
        return true;
    }

private:

    Bus(const Bus &);
    Bus &operator = (const Bus &);

private:

    FrameSourceAdder *adder;
    PausableSource *pausableSource;
    VolumeMeter *volumeMeter;

};


}  // namespace roundbeetle
