/*  WaveFileBuffer.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/WaveFileBuffer.h>


using namespace roundbeetle;


WaveFileBuffer::WaveFileBuffer()
:   spec(),
    sampleBuffer(NULL),
    numSamplesInBuffer(0)
{
}


WaveFileBuffer::WaveFileBuffer(const std::string &wavFilePath)
:   spec(),
    sampleBuffer(NULL),
    numSamplesInBuffer(0)
{
    init(wavFilePath);
}


void
WaveFileBuffer::init(const std::string &wavFilePath)
{
    Uint8 *byteBuffer = NULL;
    Uint32 numBytesInBuffer = 0;
    if (SDL_LoadWAV(wavFilePath.c_str(), &spec, &byteBuffer, &numBytesInBuffer) != &spec)
        throw std::string(SDL_GetError());
    if (byteBuffer == NULL)
        throw std::string("SDL_LoadWAV() did not allocate buffer");
    if (spec.format != AUDIO_S16)
        throw std::string("wave file format is not AUDIO_S16 (signed 16 bits)");
    if (spec.channels != 1)
        throw std::string("wave file is not mono");
    if (numBytesInBuffer % 2 != 0)
        throw std::string("wave file has odd number of bytes");

    sampleBuffer = reinterpret_cast<Sint16 *>(byteBuffer);
    numSamplesInBuffer = size_t(numBytesInBuffer) / 2;
}


WaveFileBuffer::~WaveFileBuffer()
{
    if (sampleBuffer != NULL)
        SDL_FreeWAV(reinterpret_cast<Uint8 *>(sampleBuffer));
}


const Sint16 *
WaveFileBuffer::getBuffer() const
{
    return sampleBuffer;
}


size_t
WaveFileBuffer::getNumSamples() const
{
    return numSamplesInBuffer;
}
