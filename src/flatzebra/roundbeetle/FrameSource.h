/*  FrameSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/Frame.h>

#include <stddef.h>


namespace roundbeetle {


class FrameSource
{
public:

    virtual ~FrameSource() {}

    virtual size_t getFrames(Frame *frameBuffer, size_t numFramesToFill) = 0;

    virtual bool isFinished() const = 0;

    virtual bool rewind() = 0;

};


}  // namespace roundbeetle
