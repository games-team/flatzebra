/*  NativeSDLSoundRenderer.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/NativeSDLSoundRenderer.h>

#include <assert.h>


using namespace roundbeetle;


static Uint16
roundUpToPowerOf2(Uint16 n)
{
    if (n == 0)
        return 1;
    --n;
    Uint16 numBits = 0;
    while (n > 0)
    {
        ++numBits;
        n >>= 1;
    }
    return 1 << numBits;
}


NativeSDLSoundRenderer::NativeSDLSoundRenderer(int _rendererFreqInHz)
:   SoundRenderer(_rendererFreqInHz),
    desired(),
    obtained(),
    numCallbackInvocations(0),
    accWallTime(0),
    recordingFile()
{
    enum { bufferSizeInMS = 10 };

    memset(&obtained, 0, sizeof(obtained));
    memset(&desired, 0, sizeof(desired));
    desired.freq = _rendererFreqInHz;
    desired.format = AUDIO_S16;
    desired.channels = 2;
    desired.samples = roundUpToPowerOf2(_rendererFreqInHz * bufferSizeInMS / 1000 * desired.channels);
    desired.callback = fillerCallback;
    desired.userdata = this;
}


NativeSDLSoundRenderer::~NativeSDLSoundRenderer()
{
}


NativeSDLSoundRenderer &  // static
NativeSDLSoundRenderer::create(int _rendererFreqInHz)
{
    assert(inst == NULL);
    NativeSDLSoundRenderer *r = new NativeSDLSoundRenderer(_rendererFreqInHz);
    inst = r;
    return *r;
}


void  // static
NativeSDLSoundRenderer::destroy()
{
    delete inst;
    inst = NULL;
}


int
NativeSDLSoundRenderer::initAudio()
{
    if (SDL_OpenAudio(&desired, &obtained) < 0)
        return -100;

    #if 0
    std::cout << "NativeSDLSoundRenderer::initAudio: obtained " << obtained.samples
              << "-sample buffer with freq " << obtained.freq << " Hz"
              << " (asked for " << desired.freq << " Hz)"
              << std::endl;
    #endif

    SDL_PauseAudio(0);  // start callback invocation
    return 0;
}


void
NativeSDLSoundRenderer::requestStop()
{
    SDL_PauseAudio(1);  // stop callback invocation
    SDL_CloseAudio();  // this takes two (2) seconds with SDL 1.2.14 on Fedora 14
}


bool
NativeSDLSoundRenderer::openRecordingFile(const std::string &filename)
{
    recordingFile.open(filename.c_str(), std::ios::out | std::ios::ate | std::ios::binary);
    return recordingFile.good();
}


bool
NativeSDLSoundRenderer::closeRecordingFile()
{
    recordingFile.close();
    return ! recordingFile.fail();
}


void
NativeSDLSoundRenderer::fillerCallback(void *userdata, Uint8 *stream, int len)
{
    reinterpret_cast<NativeSDLSoundRenderer *>(userdata)->fill(stream, size_t(len));
}


void
NativeSDLSoundRenderer::fill(Uint8 *stream, int len)
{
    #ifdef MEASURE_SOUND_RENDERER_PERFORMANCE
    ++numCallbackInvocations;
    timeval start;
    gettimeofday(&start, NULL);
    #endif

    Frame *frameBuffer = reinterpret_cast<Frame *>(stream);
    size_t numFramesToFill = size_t(len / 4);
    //std::cout << SDL_GetTicks() << ": numFramesToFill=" << numFramesToFill << std::endl;
    getFramesFromTopSource(frameBuffer, numFramesToFill);

    #ifdef MEASURE_SOUND_RENDERER_PERFORMANCE
    timeval finish;
    gettimeofday(&finish, NULL);
    accWallTime += (finish.tv_sec + finish.tv_usec / 1e6) - (start.tv_sec + start.tv_usec / 1e6);
    if (numCallbackInvocations % 100 == 0)
        std::cout << numCallbackInvocations << ": " << (accWallTime / numCallbackInvocations) * 1000.0 << " ms"
                << ", " << topFrameSource.getNumOverflows()
                << std::endl;
    #endif
}


void
NativeSDLSoundRenderer::record(Frame *frameBuffer, size_t numFramesToFill)
{
    if (recordingFile.good())
    {
        recordingFile.write((char *) frameBuffer, sizeof(frameBuffer[0]) * numFramesToFill);
        recordingFile.flush();
    }
}
