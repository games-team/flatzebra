/*  VolumeMeter.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/VolumeMeter.h>

#include <roundbeetle/SoundRenderer.h>


namespace roundbeetle {


VolumeMeter::VolumeMeter(float _windowLengthInSeconds, FrameSource *_src)
:   mutex(SDL_CreateMutex()),
    window(),
    src(_src),
    frameCounter(0),
    currentRMSLinVols()
{
    window.resize(std::max(size_t(_windowLengthInSeconds * SoundRenderer::freq()), size_t(1)));
    currentRMSLinVols[0] = currentRMSLinVols[1] = 0;
}


//virtual
VolumeMeter::~VolumeMeter()
{
    delete src;

    SDL_DestroyMutex(mutex);
}


size_t  //virtual
VolumeMeter::getFrames(Frame *frameBuffer, size_t numFramesToFill)
{
    size_t numObtained = src->getFrames(frameBuffer, numFramesToFill);
    if (numObtained == 0)
        return 0;

    const size_t winLen = window.size();

    for (size_t i = 0; i < numObtained; ++i, ++frameCounter)
    {
        size_t indexInWindow = frameCounter % winLen;
        window[indexInWindow] = frameBuffer[i];

        if (indexInWindow == winLen - 1)  // if window filled
            computeVolumes();
    }

    return numObtained;
}


void
VolumeMeter::getCurrentRMSVolumes(float &leftLinVol, float &rightLinVol) const
{
    AutoLocker locker(mutex);
    leftLinVol  = currentRMSLinVols[0];
    rightLinVol = currentRMSLinVols[1];
}


bool  //virtual
VolumeMeter::isFinished() const
{
    return src->isFinished();
}


bool  //virtual
VolumeMeter::rewind()
{
    if (! src->rewind())
        return false;
    frameCounter = 0;
    return true;
}


// http://en.wikipedia.org/wiki/Root_mean_square
//
void
VolumeMeter::computeVolumes()
{
    float sumsOfSquares[2] = { 0, 0 };  // one sum for each channel
    const size_t winLen = window.size();
    for (size_t i = 0; i < winLen; ++i)
    {
        const Frame &frame = window[i];
        float left = frame.left, right = frame.right;
        sumsOfSquares[0] += left * left;
        sumsOfSquares[1] += right * right;
    }

    AutoLocker locker(mutex);
    currentRMSLinVols[0] = sqrtf(sumsOfSquares[0] / winLen) / 32767;
    currentRMSLinVols[1] = sqrtf(sumsOfSquares[1] / winLen) / 32767;
}


}  // namespace roundbeetle
