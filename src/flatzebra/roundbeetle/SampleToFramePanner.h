/*  SampleToFramePanner.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/FrameSource.h>

#include <vector>


namespace roundbeetle {


class SampleSource;
class ClientObject;


class SampleToFramePanner : public FrameSource
{
public:

    // Preserves sum of squares of linear volumes.
    //
    static const float monoToStereoAttenuation;

    // _src: must come from new. Destroyed by this object's destructor.
    // _clObj: may be null. Ownership not transferred to this object.
    //
    SampleToFramePanner(SampleSource *_src, ClientObject *_clObj);

    // Destroys the SampleSource object passed to the constructor.
    //
    virtual ~SampleToFramePanner();

    virtual size_t getFrames(Frame *frameBuffer, size_t numFramesToFill) override;

    virtual bool isFinished() const override;

    virtual bool rewind() override;

    SampleSource *getChild();

    void removeChild();

private:

    SampleToFramePanner(const SampleToFramePanner &);
    SampleToFramePanner & operator = (const SampleToFramePanner &);

protected:

    SampleSource *src;
    ClientObject *clObj;
    std::vector<Sint16> monoBuf;

};


}  // namespace roundbeetle
