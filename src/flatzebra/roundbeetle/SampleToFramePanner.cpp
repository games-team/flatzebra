/*  SampleToFramePanner.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/SampleToFramePanner.h>

#include <roundbeetle/SampleSource.h>
#include <roundbeetle/ClientObject.h>

#include <iostream>
#include <math.h>
#include <assert.h>


namespace roundbeetle {


const float SampleToFramePanner::monoToStereoAttenuation = sqrtf(0.5f);


SampleToFramePanner::SampleToFramePanner(SampleSource *_src, ClientObject *_clObj)
:   src(_src),
    clObj(_clObj),
    monoBuf()
{
    assert(_src != NULL);
}


//virtual
SampleToFramePanner::~SampleToFramePanner()
{
    delete src;
}


size_t  //virtual
SampleToFramePanner::getFrames(Frame *frameBuffer, size_t numFramesToFill)
{
    if (numFramesToFill > monoBuf.size())
        monoBuf.resize(numFramesToFill);

    //std::cout << "SampleToFramePanner::getFrames: requesting " << numFramesToFill << " samples" << std::endl;
    size_t numFramesObtained = src->getSamples(&monoBuf.front(), numFramesToFill);

    float leftPan, rightPan;
    Vec objPos;

    // Get object position if any.
    // No need to lock renderer mutex, because getFrames() is already
    // called under that mutex.
    //
    if (clObj != NULL && clObj->getPosUnsafe(objPos))
    {
        // TODO: add mic pos and direction.
        // TODO: add rolloff

        if (objPos.squaredLength() < 1e-6f)
            leftPan = rightPan = monoToStereoAttenuation;
        else
        {
            float radians = atan2f(objPos.y, objPos.x);
            //SHOWNL(radians);
            float t = (cosf(radians) + 1) / 2;  // 0..1
            leftPan  = sqrtf(1 - t);
            rightPan = sqrtf(t);
            /*std::cout << "SampleToFramePanner::getFrames: " << radians * 180 / M_PI << " deg"
                      << ", t=" << t << ", leftPan=" << leftPan << ", rightPan=" << rightPan << std::endl;*/
        }
        assert(fabsf((leftPan * leftPan + rightPan * rightPan) - 1.0f) < 0.001f);
    }
    else
    {
        leftPan = rightPan = monoToStereoAttenuation;
    }

    //printf("leftPan=%f, rightPan=%f\n", leftPan, rightPan);

    size_t f;
    for (f = 0; f < numFramesObtained; ++f)
    {
        frameBuffer[f].left  = leftPan * monoBuf[f];
        frameBuffer[f].right = rightPan * monoBuf[f];
    }

    return numFramesObtained;
}


bool  //virtual
SampleToFramePanner::isFinished() const
{
    return src == NULL || src->isFinished();
}


bool  //virtual
SampleToFramePanner::rewind()
{
    if (src == NULL)
        return false;
    return src->rewind();
}


SampleSource *
SampleToFramePanner::getChild()
{
    return src;
}


void
SampleToFramePanner::removeChild()
{
    src = NULL;
}


}  // namespace roundbeetle
