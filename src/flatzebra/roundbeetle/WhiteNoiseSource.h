/*  WhiteNoiseSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/SampleSource.h>


namespace roundbeetle {


class WhiteNoiseSource : public SampleSource
{
public:

    // _toneFreq; frequency in Hz of the tone to generate.
    // _linVol: 0..1
    // _durationInSeconds: if zero, infinite.
    //
    WhiteNoiseSource(float _linVol, float _durationInSeconds);

    // Returns mono PCM-16 signal.
    //
    virtual size_t getSamples(Sint16 *dest, size_t numRequested) override;

    virtual bool isFinished() const override;

    virtual bool rewind() override;

private:

    size_t sampleCounter;
    const size_t maxNumSamples;
    const int minSampleValue;
    const int intervalSize;

};


}  // namespace roundbeetle
