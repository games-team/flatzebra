/*  SquareWaveSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/SampleSource.h>

#include <assert.h>
#include <math.h>


namespace roundbeetle {

class FrequencyFunction;


class SquareWaveSource : public SampleSource
{
public:

    // _freq: frequency in Hz (20..20000) of the tone to generate.
    // _linVol: 0..1
    // _rendererFreq: frequency in Hz of the renderer buffer (e.g. 22050 Hz).
    // _durationInSeconds: if zero, infinite.
    //
    SquareWaveSource(float _freq, float _linVol, float _durationInSeconds);

    // _frequencyFunction: object with a getNumSamplesPerPeriod() method that
    // gives the number of samples as a function of the sample counter.
    // This constructor calls that method to get the initial number of
    // samples per period.
    // This object keeps a pointer to _frequencyFunction, but does not
    // destroy it.
    // Other parameters: see previous constructor.
    //
    SquareWaveSource(FrequencyFunction &_frequencyFunction, float _linVol, float _durationInSeconds);

    // Returns mono PCM-16 signal.
    //
    virtual size_t getSamples(Sint16 *dest, size_t numRequested) override;

    virtual bool isFinished() const override;

    virtual bool rewind() override;

    // Change the frequency in Hz (20..20000) of the square wave.
    // Takes effect at the end of the current wave period.
    // Disables any FrequencyFunction that may currently be active.
    //
    void setFrequency(float newFreq);

private:

    SquareWaveSource(const SquareWaveSource &);
    SquareWaveSource &operator = (const SquareWaveSource &);

private:

    size_t sampleCounter;  // counts samples since beginning
    size_t samplesInPeriodCounter;  // counts samples since beginning of current period
    const size_t maxNumSamples;
    size_t samplesPerPeriod;
    FrequencyFunction *frequencyFunction;  // may be null
    size_t pendingSamplesPerPeriod;
    const Sint16 sampleValue;  // absolute value of the samples returned

};


}  // namespace roundbeetle
