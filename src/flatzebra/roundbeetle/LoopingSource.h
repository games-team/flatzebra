/*  LoopingSource.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/SampleSource.h>

#include <assert.h>


namespace roundbeetle {


class LoopingSource : public SampleSource
{
public:

    // _src: must come from new. Will be destroyed by destructor.
    // _numLoops: number of loops to do (0 means infinity).
    //
    LoopingSource(SampleSource *_src, size_t _numLoops = 0);

    virtual ~LoopingSource();

    // Returns mono PCM-16 signal.
    //
    virtual size_t getSamples(Sint16 *dest, size_t numRequested) override;

    virtual bool isFinished() const override;

    virtual bool rewind() override;

private:

    LoopingSource(const LoopingSource &);
    LoopingSource & operator = (const LoopingSource &);

private:

    SampleSource *src;
    size_t numLoops;  // number of loops to do (0 means infinity)
    size_t loopCounter;  // loops done until now

};


}  // namespace roundbeetle
