/*  SineSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/SineSource.h>


namespace roundbeetle {


SineSource::SineSource(float _toneFreq, float _linVol, float _durationInSeconds)
:   sampleCounter(0),
    maxNumSamples(size_t(_durationInSeconds * SoundRenderer::freq())),
    sinFactor(32767 * _linVol),
    sinArg(2.0f * float(M_PI) * _toneFreq / SoundRenderer::freq())
{
    assert(_toneFreq >= 20 && _toneFreq <= 20000);
    assert(_linVol >= 0 && _linVol <= 1);
}


size_t
SineSource::getSamples(Sint16 *dest, size_t numRequested)
{
    if (dest == NULL)
        return 0;

    size_t numSamplesLeft = maxNumSamples - sampleCounter;
    size_t numDelivered = std::min(numRequested, numSamplesLeft);
    for (size_t i = 0; i < numDelivered; ++i, ++sampleCounter)
        dest[i] = Sint16(sinFactor * sinf(sampleCounter * sinArg));

    return numDelivered;
}


bool
SineSource::isFinished() const
{
    return sampleCounter >= maxNumSamples;
}


bool
SineSource::rewind()
{
    sampleCounter = 0;
    return true;
}


}  // namespace roundbeetle
