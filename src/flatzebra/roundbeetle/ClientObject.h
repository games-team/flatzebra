/*  ClientObject.h

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#pragma once

#include <flatzebra/roundbeetle/Vec.h>


namespace roundbeetle {


class ClientObject
{
public:

    ClientObject(bool isPos = false, const Vec &initPos = Vec())
    :   isPositioned(isPos),
        pos(initPos)
    {
    }

    // Lock the renderer mutex.
    //
    void setPos(const Vec &newPos)
    {
        AutoLocker a(SoundRenderer::getMutex());
        pos = newPos;
    }

    // Lock the renderer mutex.
    //
    void setPos(float x, float y, float z)
    {
        setPos(Vec(x, y, z));
    }

    // Does not lock the renderer mutex.
    //
    bool getPosUnsafe(Vec &dest) const
    {
        if (! isPositioned)
            return false;
        dest = pos;
        return true;
    }

private:

    bool isPositioned;
    Vec pos;

};


}  // namespace roundbeetle
