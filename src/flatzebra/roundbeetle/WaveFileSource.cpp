/*  WaveFileSource.cpp

    flatzebra - SDL-based sound renderer
    Copyright (C) 2011 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this program; if not, write to the Free
    Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#include <roundbeetle/WaveFileSource.h>


using namespace roundbeetle;


WaveFileSource::WaveFileSource(const WaveFileBuffer &_waveFileBuffer)
:   SampleSource(),
    waveFileBuffer(_waveFileBuffer),
    sampleCounter(0)
{
}


//virtual
WaveFileSource::~WaveFileSource()
{
}


size_t  //virtual
WaveFileSource::getSamples(Sint16 *dest, size_t numRequested)
{
    //std::cout << "WaveFileSource::getSamples(" << dest << ", " << numRequested << "): numSamplesInBuffer=" << numSamplesInBuffer << std::endl;
    if (dest == NULL || waveFileBuffer.getBuffer() == NULL)
        return 0;  // anomaly
    if (sampleCounter >= waveFileBuffer.getNumSamples())
        return 0;  // out of samples

    size_t numSamplesAvail = waveFileBuffer.getNumSamples() - sampleCounter;
    size_t numSamplesReturned = std::min(numSamplesAvail, numRequested);
    //std::cout << "WaveFileSource at " << this << " copying from " << sampleCounter << " for " << numSamplesReturned << std::endl;
    memcpy(dest, waveFileBuffer.getBuffer() + sampleCounter, numSamplesReturned * 2);
    sampleCounter += numSamplesReturned;

    return numSamplesReturned;
}


bool  //virtual
WaveFileSource::isFinished() const
{
    return sampleCounter >= waveFileBuffer.getNumSamples() || waveFileBuffer.getBuffer() == NULL;
}


bool  //virtual
WaveFileSource::rewind()
{
    sampleCounter = 0;
    return true;
}
