/*  Joystick.cpp - Joystick or game pad encapsulation.

    flatzebra - Generic 2D Game Engine library
    Copyright (C) 1999-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "Joystick.h"

using namespace std;
using namespace flatzebra;


Joystick::Joystick()
:   joystick(NULL),
    previousButtonStates(),
    currentButtonStates(),
    xAxis(),
    yAxis()
{
    xAxis[0] = xAxis[1] = 0;
    yAxis[0] = yAxis[1] = 0;

    if (SDL_Init(SDL_INIT_JOYSTICK) == 0 && SDL_NumJoysticks() > 0)
    {
        joystick = SDL_JoystickOpen(0);
        if (joystick != NULL)
        {
            size_t numButtons = size_t(SDL_JoystickNumButtons(joystick));
            previousButtonStates.resize(numButtons, false);
            currentButtonStates.resize(numButtons, false);
        }
    }
}


bool
Joystick::isJoystickAvailable() const
{
    return joystick != NULL;
}


size_t
Joystick::getNumButtons() const
{
    return currentButtonStates.size();
}


Joystick::~Joystick()
{
    if (joystick != NULL)
        SDL_JoystickClose(joystick);
}


bool
Joystick::getButton(int buttonNumber) const
{
    if (joystick == NULL)  // if no joystick detected
        return false;
    if (buttonNumber < 0 || size_t(buttonNumber) >= getNumButtons())
        return false;
    return currentButtonStates[buttonNumber];
}


bool
Joystick::buttonJustPressed(int buttonNumber) const
{
    if (!getButton(buttonNumber))
        return false;  // not pressed, or no joystick, or invalid button number
    return !previousButtonStates[size_t(buttonNumber)];  // true if was not pressed, an instant before
}


int
Joystick::getXAxisDisplacement(int stickNumber) const
{
    if (xAxis[stickNumber] < -AXIS_THRESHOLD)
        return -1;
    if (xAxis[stickNumber] > +AXIS_THRESHOLD)
        return +1;
    return 0;
}


int
Joystick::getYAxisDisplacement(int stickNumber) const
{
    if (yAxis[stickNumber] < -AXIS_THRESHOLD)
        return -1;
    if (yAxis[stickNumber] > +AXIS_THRESHOLD)
        return +1;
    return 0;
}


int
Joystick::getTriggerValue(int triggerNumber) const
{
    return SDL_JoystickGetAxis(joystick, triggerNumber);
}


void
Joystick::update()
{
    if (joystick == NULL)
        return;

    // Remember current joystick button states for next frame.
    size_t numButtons = getNumButtons();
    for (size_t i = 0; i < numButtons; ++i)
        previousButtonStates[i] = currentButtonStates[i];

    // Update joystick button states.
    SDL_JoystickUpdate();

    for (size_t i = 0; i < numButtons; ++i)
        currentButtonStates[i] = (SDL_JoystickGetButton(joystick, i) != 0);

    xAxis[0] = SDL_JoystickGetAxis(joystick, MAIN_X);
    yAxis[0] = SDL_JoystickGetAxis(joystick, MAIN_Y);
    xAxis[1] = SDL_JoystickGetAxis(joystick, SECONDARY_X);
    yAxis[1] = SDL_JoystickGetAxis(joystick, SECONDARY_Y);
}
