/*  KeyState.h - Representation of the state of a keyboard key

    flatzebra - Generic 2D Game Engine library
    Copyright (C) 2003-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef _H_KeyState
#define _H_KeyState

#include <SDL_keycode.h>


namespace flatzebra {


class KeyState
{
public:

    KeyState(SDL_Keycode k0, SDL_Keycode k1 = SDLK_UNKNOWN)
      : key(k0), otherKey(k1), now(false), before(false)
    {
    }

    // If 'keysym' corresponds to one of the keys represented by this KeyState object,
    // then the button is now considered pressed.
    //
    void check(SDL_Keycode keysym, bool pressed)
    {
        if (keysym == key || keysym == otherKey)
            now = pressed;
    }

    bool isPressed() const
    {
        return now;
    }

    bool justPressed() const
    {
        return !before && now;
    }

    // Should be called periodically, typically once per animation frame.
    //
    void remember()
    {
        before = now;
    }

private:

    SDL_Keycode key;
    SDL_Keycode otherKey;
    bool now;     // pressed now or not
    bool before;  // pressed before or not

    /*  Forbidden operations:
    */
    KeyState(const KeyState &x);
    KeyState &operator = (const KeyState &x);
};


}  // namespace flatzebra


#endif  /* _H_KeyState */
