/*  GameEngine.cpp - Abstract class representing an SDL Game Engine.

    flatzebra - Generic 2D Game Engine library
    Copyright (C) 1999-2024 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

/*
    CAUTION:
    Every primitive drawing operation in this engine, except those done
    by SDL2_gfx, must be preceded by calls to SDL_SetRenderDrawBlendMode()
    and SDL_SetRenderDrawColor(), because SDL2_gfx typically makes such calls
    before doing its work.
    See the source code for aalineRGBA() for example.
    Utility function prepareDrawingOp() should be used for this.
*/

#include <flatzebra/GameEngine.h>

#include <assert.h>

using namespace std;
using namespace flatzebra;


GameEngine::GameEngine(Couple screenSizeInPixels,
                       const string &wmCaption,
                       bool fullScreen,
                       bool _processActiveEvent,
                       bool _useAcceleratedRendering)
  : theScreenSizeInPixels(screenSizeInPixels),
    window(NULL),
    renderer(NULL),
    fullScreenLetterboxColor(),
    fixedWidthFontPixmap(NULL),
    usingFullScreen(fullScreen),
    processActiveEvent(_processActiveEvent),
    useAcceleratedRendering(_useAcceleratedRendering)
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
        throw string("SDL_Init: ") + SDL_GetError();

    Uint32 flags = (fullScreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
    window = SDL_CreateWindow(wmCaption.c_str(),
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              screenSizeInPixels.x, screenSizeInPixels.y,
                              flags);
    if (!window)
        throw string("SDL_CreateWindow: ") + SDL_GetError();

    renderer = SDL_CreateRenderer(window, -1,
                                  useAcceleratedRendering
                                        ? SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
                                        : SDL_RENDERER_SOFTWARE);
    if (!renderer)
        throw string("SDL_CreateRenderer: ") + SDL_GetError();

    // Set blend mode and default background color.
    //
    SDL_Color black = { 0, 0, 0, 255 };
    prepareDrawingOp(black);

    // This ensures that in full screen mode, the whole physical screen is used.
    //
    SDL_RenderSetLogicalSize(renderer, screenSizeInPixels.x, screenSizeInPixels.y);

    // This could make the scaled rendering used by full screen mode look smoother,
    // but text written with the fixedWidthFontPixmap shows tiny lines between the glyphs.
    //
    //SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

    // Prepare a fixed width font pixmap:
    //
    string fontFilePath = getDirPathFromEnv(PKGPIXMAPDIR, "PKGPIXMAPDIR") + "font_13x7.xpm";
    fixedWidthFontPixmap = createTextureFromFile(fontFilePath);  // may throw

    fullScreenLetterboxColor.r = 0;
    fullScreenLetterboxColor.g = 0;
    fullScreenLetterboxColor.b = 0;
    fullScreenLetterboxColor.a = 255;
}


GameEngine::~GameEngine()
{
    SDL_DestroyTexture(fixedWidthFontPixmap);
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
}


void
GameEngine::prepareDrawingOp(const SDL_Color &color)
{
    int err = SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    if (err != 0)
        throw string("GameEngine::prepareDrawingOp: SDL_SetRenderDrawBlendMode/SDL_BLENDMODE_BLEND: ") + SDL_GetError();

    err = SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    if (err != 0)
        throw string("GameEngine::prepareDrawingOp: SDL_SetRenderDrawColor: ") + SDL_GetError();
}


// Returns the non-zero index in palette.colors[] of a color that
// is equal to palette.colors[0], or 0 if none is found.
// Returns 0 if the palette is empty or contains only one color.
//
static int
findColorSameAsFirstColor(const SDL_Palette &palette)
{
    for (int i = 1; i < palette.ncolors; ++i)
        if (memcmp(&palette.colors[i], &palette.colors[0], sizeof(palette.colors[0])) == 0)  // '==' rejected by g++ 9.4.0 (!)
            return i;
    return 0;
}


// Checks if the first color of the palette of 's', which is assumed
// to be the transparent color, is the same as any other color in the palette.
// If it is, the first color's alpha is changed to make the transparent color
// different from the rest of the palette.
// Without this patch, XPMs and PNGs have been seen to use the wrong color
// as the transparent one.
// Returns false if it was not possible to find a color that does not clash with
// the rest of the palette, which is very unlikely.
// This patch has been found to be necessary with SDL2 2.0.10 and/or SDL2_image 2.0.5.
//
static bool
patchSDLSurfaceTransparencyIssue(SDL_Surface &s)
{
    SDL_Palette *palette = s.format->palette;
    if (palette == NULL)  // if no palette
        return true;  // nothing to do
    const Uint8 originalAlpha = palette->colors[0].a;
    for (;;)
    {
        // Assume 1st color is transparent color.
        // Check if other color is the same.
        //
        int indexOfDupeColor = findColorSameAsFirstColor(*palette);
        if (indexOfDupeColor == 0)  // if no dupe, or if only 1 color in palette
            return true;  // no patch needed
        
        // Change transparent color until different from rest of palette.
        // Fail if alpha value cycles back to original one.
        //
        if (++palette->colors[0].a == originalAlpha)
            return false;  // failed to find alpha that does not clash with rest of palette
    }
}


SDL_Texture *
GameEngine::createTextureFromFile(const char *filePath)
{
    SDL_Surface *s = IMG_Load(filePath);
    if (!s)
        throw string("GameEngine::createTextureFromFile: IMG_Load failed on '") + filePath + "': " + SDL_GetError();

    patchSDLSurfaceTransparencyIssue(*s);

    SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, s);
    SDL_FreeSurface(s);
    if (!tex)
        throw string("GameEngine::createTextureFromFile: SDL_CreateTextureFromSurface failed for '")
              + filePath + "': " + SDL_GetError();
    return tex;
}


SDL_Texture *
GameEngine::createTextureFromFile(const string &filePath)
{
    return createTextureFromFile(filePath.c_str());
}


Couple
GameEngine::getTextureSize(SDL_Texture *texture)
{
    Couple size;
    int err = SDL_QueryTexture(texture, NULL, NULL, &size.x, &size.y);
    if (err != 0)
        throw string("SDL_QueryTexture: ") + SDL_GetError();
    return size;
}


void
GameEngine::setTextureAlphaMod(SDL_Texture *texture, Uint8 alpha)
{
    int err = SDL_SetTextureAlphaMod(texture, alpha);
    if (err != 0)
        throw string("setTextureAlphaMod: ") + SDL_GetError();
}


string
GameEngine::setFullScreenMode(bool fullScreenMode)
{
    // SDL_WINDOW_FULLSCREEN would cause the full screen mode to change the hardware video mode
    // (which takes a few seconds) and would cause the image to be stretched horizontally.
    // (Observed with SDL2 2.0.10.)
    //
    int err = SDL_SetWindowFullscreen(window, fullScreenMode ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
    if (err != 0)
        return string("SDL_SetWindowFullscreen: ") + SDL_GetError();
    usingFullScreen = fullScreenMode;
    return string();
}


void
GameEngine::copyPixmap(SDL_Texture *src, Couple dest) const
{
    assert(src);
    SDL_Rect dstrect = { dest.x, dest.y, 0, 0 };
    int err = SDL_QueryTexture(src, NULL, NULL, &dstrect.w, &dstrect.h);
    if (err != 0)
        throw string("SDL_QueryTexture: ") + SDL_GetError();
    err = SDL_RenderCopy(renderer, src, NULL, &dstrect);
    if (err != 0)
        throw string("SDL_RenderCopy: ") + SDL_GetError();
}


void GameEngine::run(int millisecondsPerFrame)
{
    for (;;)
    {
        Uint32 lastTime = SDL_GetTicks();

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_KEYDOWN)
                processKey(event.key.keysym.sym, true);   // virtual function
            else if (event.type == SDL_KEYUP)
                processKey(event.key.keysym.sym, false);  // virtual function
            else if (processActiveEvent
                     && event.type == SDL_WINDOWEVENT
                     && event.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
            {
                // App is now inactive (iconified or lost focus), so
                // go wait for reactivation, i.e., stop calling tick().
                // This avoids using the CPU while the app is not active.
                //
                if (!waitForReactivation())
                    return;  // wait ended with quit or with wait error
            }
            else if (event.type == SDL_QUIT)
                return;
        }

        startDrawingFrame();

        if (!tick())  // virtual function
            return;

        flip();  // "flip" the screen buffers

        // Pause for the rest of the current animation frame.
        Uint32 limit = lastTime + millisecondsPerFrame;
        Uint32 delay = limit - SDL_GetTicks();
        if (delay <= (Uint32) millisecondsPerFrame)
            SDL_Delay(delay);
    }
}


void
GameEngine::startDrawingFrame()
{
    // This operation is necessary to set the color that is used for
    // the letterboxing in full screen mode.
    prepareDrawingOp(fullScreenLetterboxColor);

    SDL_RenderClear(renderer);
}


void
GameEngine::flip()
{
    SDL_RenderPresent(renderer);
}


bool
GameEngine::waitForReactivation()
{
    // Virtual call: notify derived class that app is now inactive.
    //
    processActivation(false);

    // Allow effect of drawing commands made by processActivation() to appear.
    //
    flip();

    // Sleep on SDL event loop until reactivation.
    //
    SDL_Event event;
    while (SDL_WaitEvent(&event))
    {
        // Ignore events other than quitting and reactivation.
        //
        switch (event.type)
        {
            case SDL_QUIT:
                processActivation(true);  // notify derived class that app is now active
                return false;  // ask caller to quit
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
                {
                    processActivation(true);
                    return true;  // ask caller to continue
                }
                break;
        }
    }
    return false;  // unexpected wait error: quit
}


/*virtual*/
void
GameEngine::processActivation(bool /*appActive*/)
{
}


void
GameEngine::loadPixmap(const char *filePath,
                       PixmapArray &pa,
                       size_t index)
{
    SDL_Texture *tex = createTextureFromFile(filePath);  // may throw std::string
    pa.setArrayElement(index, tex);
    pa.setImageSize(getTextureSize(tex));
}


void
GameEngine::loadPixmap(const string &filePath,
                       PixmapArray &pa,
                       size_t index)
{
    loadPixmap(filePath.c_str(), pa, index);
}


void
GameEngine::writeString(const char *s, Couple pos)
{
    assert(fixedWidthFontPixmap != NULL);
    if (s == NULL)
        return;
    Couple fontDim = getFontDimensions();
    SDL_Rect dest = { pos.x, pos.y, fontDim.x, fontDim.y };
    for (size_t i = 0; s[i] != '\0'; i++, dest.x += fontDim.x)
    {
        unsigned char c = (unsigned char) s[i];
        if (c < 32 || (c >= 127 && c <= 160))  // if ctrl char or undef char
            c = 32;  // replace by space

        /*  Compute the subrectangle of fixedWidthFontPixmap that
            contains the character:
        */
        int x = (c % 16) * fontDim.x;
        int y = (c - 32) / 16;
        if (y >= 8)
            y -= 2;
        y *= fontDim.y;

        SDL_Rect srcRect = { x, y, fontDim.x, fontDim.y };
        SDL_RenderCopy(renderer, fixedWidthFontPixmap, &srcRect, &dest);
    }
}


void
GameEngine::writeStringCentered(const char *s, Couple pos)
{
    Couple fontDim = getFontDimensions();
    Couple stringSizeInPixels(strlen(s) * fontDim.x, fontDim.y);
    writeString(s, pos - stringSizeInPixels / 2);
}


void
GameEngine::writeStringXCentered(const char *s, Couple pos)
{
    Couple fontDim = getFontDimensions();
    int stringWidthInPixels = strlen(s) * fontDim.x;
    Couple adjustedPos(pos.x - stringWidthInPixels / 2, pos.y);
    writeString(s, adjustedPos);
}


void
GameEngine::writeStringRightJustified(const char *s, Couple pos)
{
    Couple fontDim = getFontDimensions();
    int stringWidthInPixels = (int) strlen(s) * fontDim.x;
    Couple adjustedPos(pos.x - stringWidthInPixels, pos.y);
    writeString(s, adjustedPos);
}


void
GameEngine::drawPixel(int x, int y, SDL_Color color)
{
    pixelRGBA(renderer,
              Sint16(x), Sint16(y),
              color.r, color.g, color.b, color.a);
}


void
GameEngine::drawLine(int x1, int y1, int x2, int y2, SDL_Color color)
{
    aalineRGBA(renderer,
               Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2),
               color.r, color.g, color.b, color.a);
}


void
GameEngine::fillRect(int x, int y, int width, int height, SDL_Color color)
{
    boxRGBA(renderer,
            Sint16(x), Sint16(y),
            Sint16(x + width), Sint16(y + height),
            color.r, color.g, color.b, color.a);
}


void
GameEngine::gfxWriteString(const char *s, Couple pos, SDL_Color color)
{
    stringRGBA(renderer,
		       Sint16(pos.x), Sint16(pos.y),
		       s,
		       color.r, color.g, color.b, color.a);
}


void
GameEngine::fillCircle(int x, int y, int radius, SDL_Color color)
{
    filledCircleRGBA(renderer,
                     Sint16(x), Sint16(y), Sint16(radius),
                     color.r, color.g, color.b, color.a);
}


void
GameEngine::sdlColorToString(char dest[], size_t destSize, SDL_Color color)
{
    snprintf(dest, destSize, "%02X%02X%02X%02X", color.r, color.g, color.b, color.a);
}


string
GameEngine::getDirPathFromEnv(const char *defaultValue, const char *envVarName)
{
    string dir;
    const char *s = envVarName ? getenv(envVarName) : NULL;
    if (s != NULL && s[0] != '\0')
        dir = s;
    else
        dir = defaultValue ? defaultValue : "";

    if (!dir.empty() && dir[dir.length() - 1] != '/')
        dir += '/';

    return dir;
}


SDL_BlendMode
GameEngine::getBlendMode() const
{
    SDL_BlendMode bm = SDL_BLENDMODE_NONE;
    int err = SDL_GetRenderDrawBlendMode(renderer, &bm);
    if (err != 0)
        throw string("GameEngine::getBlendMode: SDL_GetRenderDrawBlendMode: ") + SDL_GetError();
    return bm;
}
